/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serverinfo.h"
#include "utils.h"

Utils::Utils()
{
}

bool Utils::IsProxy( const ServerInfo& info ) {
    if ( info.rules.contains( Rule( "*QTV" ) ) ) {
        return true;
    }

    int idx = info.rules.indexOf( Rule( "*version" ) );
    if ( idx == -1 ) {
        return false;
    }
    Rule r( info.rules.at( idx ) );
    if ( r.value.startsWith( "qwfwd" ) || r.value.startsWith( "QTV" ) ) {
        return true;
    }
    return false;
}

bool Utils::IsQTV( const ServerInfo& info ) {
    if ( info.rules.contains( Rule( "*QTV" ) ) ) {
        return true;
    }

    int idx = info.rules.indexOf( Rule( "*version" ) );
    if ( idx == -1 ) {
        return false;
    }
    Rule r( info.rules.at( idx ) );
    if ( r.value.startsWith( "QTV" ) ) {
        return true;
    }
    return false;
}
