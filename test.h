/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef TEST_H
#define TEST_H

#include <QTcpSocket>
#include "protocol.h"
#include <QTimer>
#include <QDataStream>
#include "serverinfo.h"
#include "qtvbrowser.h"

class Fruny: public QTcpSocket {
    Q_OBJECT
public:
    Fruny(QObject *parent = 0):
        QTcpSocket(parent),dataSize( 0 ) {
        connect(this, SIGNAL(readyRead()), SLOT(IncomingMissile()));
        QTimer::singleShot(10000, this, SLOT(test()));
        count = dataSize = 0;
    }

    QList<ServerInfo> bizarro;
    ServerInfo info;
    quint32 dataSize;
    quint32 count;

public slots:
    void qtvbrowser( const QList<QTVBrowser::Source>& sources ) {
        foreach ( QTVBrowser::Source source, sources ) {
            qDebug() << source.streamNumber << source.host.address << source.host.port;
        }
    }

    void IncomingMissile( void ) {
        for ( ;; ) {
            // Don't read unless we've got it all
            qint64 bytes = bytesAvailable();
            QDataStream stream( this );
            if ( !dataSize ) {
                if ( bytes < sizeof( quint32 ) ) {
                    return;
                }
                stream >> dataSize;
            }
            bytes = bytesAvailable();
            if ( bytes < dataSize ) {
                return;
            }

            quint32 expectedBytes = bytes - dataSize;
            for ( ; ; ) {
                bytes = bytesAvailable();
                if ( bytes <= expectedBytes ) {
                    dataSize = 0;
                    break;
                }
                if ( stream.atEnd() ) {
                    dataSize = 0;
                    return;
                }

                quint8 c;
                stream >> c;

                switch(c) {
                case SvcInfo:
                {
                    stream >> info;
                    qDebug() << "Client Received <<" << info.hostname << info.countryCode << ++count;
                    break;
                }

                case SvcInfoAll:
                {
                    QList<ServerInfo> dados;
                    stream >> dados;
                    foreach ( ServerInfo i, dados ) {
                        qDebug() << "Client Received <<" << i.hostname << i.name << i.state;
                    }
    //                QTimer::singleShot(1000, this, SLOT(test()));
                    break;
                }

                case SvcDisconnect:
                default:
                {
                    qDebug() << "BAD READ ON CLIENT";
                    disconnectFromHost();
                    break;
                }
                }
            }
        }
    }

    void test(void ) {
//        return;
//        for ( int i = 0; i < 5; ++i ) {
//        QByteArray data;
//        QDataStream ds( &data, QIODevice::WriteOnly );
//        ds << (quint32)0;
//        for ( int j = 0; j < 1000; ++j ) {
//            ds << (quint8)ClcGet << QHostAddress( "200.98.160.131" ) << (quint16)28501;
//        }
//        ds.device()->seek( 0 );
//        ds << (quint32)( data.size() - sizeof( quint32 ) );
//        write( data );
//        dataSize = 0;
//        }
    }

};

#endif // TEST_H
