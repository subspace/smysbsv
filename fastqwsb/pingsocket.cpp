/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "pingsocket.h"
#include <QTimer>

PingSocket::PingSocket(QObject *parent) :
    QUdpSocket(parent),
    timer( new QTimer( this ) )
{
    connect( timer, &QTimer::timeout, this, &PingSocket::CheckTimedoutRequests );
    connect( this, &PingSocket::readyRead, this, &PingSocket::ParseServerResponse );
}

static QByteArray pingPacket( "\xff\xff\xff\xffk" );
static QByteArray ackPacket( "l" );

void PingSocket::PingServer( const QHostAddress& server, quint16 port ) {
    if ( !queued.size() && !active.size() ) {
        emit Pinging( true );
    }
    queued.push_back( ActivePing( server, port ) );
    FillFreeSlots();
    timer->start( 200 );
}

void PingSocket::FillFreeSlots( void ) {
    QDateTime now = QDateTime::currentDateTime();

    if ( active.size() < MaxActivePingRequests && queued.size() ) {
        int free_slots = MaxActivePingRequests - active.size();
        int queue_size = queued.size();
        for ( int i = 0; i < free_slots && i < queue_size; ++i ) {
            ActivePing p = queued.takeAt( 0 );
            if ( !p.replyTime.isNull() ) {
                if ( p.replyTime.msecsTo( now ) < IntervalBetweenRequests ) {
                    queued.push_back( p );
                    continue;
                }
            }
            p.requestTime = now;
            active.push_back( p );
            writeDatagram( pingPacket, p.addr, p.port );
        }
    }
}

quint16 PingSocket::CalculateAveragePing( const quint16 pings[] ) {
    quint16 acc = 0;
    for ( int i = 0; i < PingRequestsForAverage; ++i ) {
        acc += pings[i];
    }
    return (quint16)int( acc / (float)PingRequestsForAverage );
}

void PingSocket::CheckTimedoutRequests( void ) {
    QDateTime now = QDateTime::currentDateTime();
    QList<ActivePing> removelist;

    // Check timedout requests
    for ( int i = 0; i < active.size(); ++i ) {
        ActivePing p = active.at( i );
        if ( p.requestTime.msecsTo( now ) > MaxWaitingTime ) {
            if ( p.numPings < PingRequestsForAverage ) { // Average ping not yet complete request another ping
                p.pings[p.numPings++] = 999;
                p.replyTime = now;
                queued.push_back( p );
            } else {
                emit ResponseTime( p.addr, p.port, CalculateAveragePing( p.pings ) );
            }
            removelist.push_back( p );
        }
    }
    // Remove timedout
    for ( int i = 0; i < removelist.size(); ++i ) {
        active.removeAll( removelist.at( i ) );
    }

    FillFreeSlots();

    if ( !active.size() && !queued.size() ) {
        emit Pinging( false );
        timer->stop();
    }
}

void PingSocket::ParseServerResponse( void ) {
    QDateTime now = QDateTime::currentDateTime();
    QHostAddress host;
    quint16 port;
    QByteArray reply( 255, Qt::Uninitialized );

    while ( hasPendingDatagrams() ) {
        readDatagram( reply.data(), 255, &host, &port );
        if ( reply.startsWith( ackPacket ) ) {
            int idx = active.indexOf( ActivePing( host, port ) );
            if ( idx == -1 ) {
                continue;
            }
            ActivePing p = active.takeAt( idx );
            if ( p.numPings < PingRequestsForAverage ) { // Average ping not yet complete request another ping
                p.pings[p.numPings++] = p.requestTime.msecsTo( now );
                p.replyTime = now;
                queued.push_back( p );
            } else {
                emit ResponseTime( p.addr, p.port, CalculateAveragePing( p.pings ) );
            }
        }
    }
    FillFreeSlots();
}
