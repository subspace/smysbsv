/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "clientsocket.h"
#include "protocol.h"
#include "serverinfo.h"
#include <QDataStream>
#include <QDebug>

ClientSocket::ClientSocket(QObject *parent) :
    QTcpSocket(parent),
    dataSize( 0 )
{
    connect( this, &ClientSocket::readyRead, this, &ClientSocket::ReadPackets );
    connect( this, &ClientSocket::disconnected, this, &ClientSocket::OnDisconnection );
    connect( this, &ClientSocket::connected, this, &ClientSocket::OnConnection );
}

void ClientSocket::QueryAllServers( void ) {
    QByteArray data;
    QDataStream ds( &data, QIODevice::WriteOnly );
    ds << (quint32)0 << (quint8)ClcGetAll;
    ds.device()->seek( 0 );
    ds << (quint32)( data.size() - sizeof( quint32 ) );
    write( data );
}

void ClientSocket::QueryOneServer( const QHostAddress &addr, quint16 port ) {
    QByteArray data;
    QDataStream ds( &data, QIODevice::WriteOnly );
    ds << (quint32)0 << (quint8)ClcGet << addr << port;
    ds.device()->seek( 0 );
    ds << (quint32)( data.size() - sizeof( quint32 ) );
    write( data );
}

void ClientSocket::OnConnection( void ) {
    dataSize = 0;
}

void ClientSocket::OnDisconnection( void ) {

}

void ClientSocket::ReadPackets( void ) {
    for (;;) {
        QDataStream stream( this );
        if ( !dataSize ) {
            if ( bytesAvailable() < sizeof( quint32 ) ) {
                return;
            }
            stream >> dataSize;
        }
        if ( bytesAvailable() < dataSize ) {
            return;
        }

        quint32 expectedBytes = bytesAvailable() - dataSize;
        for (;;) {
            if ( bytesAvailable() <= expectedBytes ) {
                dataSize = 0;
                break;
            }
            if ( stream.atEnd() ) {
                dataSize = 0;
                return;
            }

            quint8 c;
            stream >> c;

            switch(c) {
            case SvcInfoAll: {
                QList<ServerInfo> infoList;
                stream >> infoList;
                emit ServerList( infoList );
                break;
            }

            case SvcInfo: {
                ServerInfo info;
                stream >> info;
                emit ServerInformation( info );
                break;
            }

            case SvcNewServer: {
                ServerInfo info;
                stream >> info;
                emit NewServer( info );
            }

            case SvcDisconnect:
            default: {
                qDebug() << "BAD READ ON CLIENT";
                disconnectFromHost();
                break;
            }
            }
        }
    }
}
