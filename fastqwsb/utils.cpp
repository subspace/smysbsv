/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "utils.h"
#include "serverinfo.h"

Utils::Utils()
{
}

QVector<QPair<int, int> > Utils::SequentialRangesFromSet( QVector<int> set ) {
    QVector<QPair<int, int> > ranges;

    if ( set.isEmpty() )
        return ranges;

    int lastIdx = -1;
    int start;
    qSort( set );
    foreach ( int idx, set ) {
         if ( lastIdx == -1 ) {
            lastIdx = idx;
            start = idx;
            continue;
        }

        if ( idx == (lastIdx + 1) ) {
            lastIdx = idx;
            continue;
        }
        ranges.push_back( QPair<int, int>( start, lastIdx ) );
        start = lastIdx = idx;
    }
    ranges.push_back( QPair<int, int>( start, lastIdx ) );

    return ranges;
}

bool Utils::IsWithinRange( int value, const QPair<int, int> &range ) {
    return ( value >= range.first && value <= range.second );
}
