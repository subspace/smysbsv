/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serversmodel.h"
#include "playersmodel.h"
#include "rulesmodel.h"
#include "allplayerssortfilterproxymodel.h"
#include "serverscolumnpicturedelegate.h"
#include "serverssortfilterproxymodel.h"
#include "allplayersmodel.h"
#include "allplayerscountrydelegate.h"
#include "dialogsettings.h"
#include "dialogconnect.h"
#include "dialogabout.h"
#include <QLabel>
#include <QDirIterator>
#include <QMessageBox>
#include <QToolButton>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    statusIcon( new QLabel( this ) ),
    work( new QLabel( this ) ),
    onlineIcon( ":/icons/png/16x16/status/user-online.png" ),
    offlineIcon( ":/icons/png/16x16/status/user-offline.png" ),
    connectIcon( ":/icons/png/22x22/actions/network-connect-3.png" ),
    disconnectIcon( ":/icons/png/22x22/actions/network-disconnect-3.png" )
{

    // Setup ui
    ui->setupUi(this);

    // Load the country flags
    QDirIterator it( ":/icons/png/22x22/intl" );
    while ( it.hasNext() ) {
        QString path = it.next();
        QString isoCode = path.section( "/", -1 ).mid( 5, 2 ).toUpper();
        QPixmap image( path );
        flags.insert( isoCode, image );
    }

    // Load the delegates
    ServersColumnPictureDelegate* cp = new ServersColumnPictureDelegate( flags, ui->tableServers->GetServersModel() );
    ui->tableServers->setItemDelegateForColumn( ServersModel::CountryColumn, cp );
    ui->tableServers->setItemDelegateForColumn( ServersModel::NeedPassColumn, cp );
    AllPlayersCountryDelegate* apd = new AllPlayersCountryDelegate( flags, ui->tableServers->GetAllPlayersModel() );
    ui->tableAllPlayers->setItemDelegateForColumn( AllPlayersModel::CountryColumn, apd );

    ui->statusBar->addWidget( statusIcon );
    ui->statusBar->addWidget( work );
    SetOnline( false );

    ui->tablePlayers->setModel( ui->tableServers->GetPlayersModel() );
    ui->tableRules->setModel( ui->tableServers->GetRulesModel() );
    ui->tableAllPlayers->setModel( ui->tableServers->GetAllPlayersModel() );

    connect( ui->tableServers->GetServersModel(), &ServersModel::Connected, this, &MainWindow::SetOnline );
    connect( ui->tableServers->GetServersModel(), &ServersModel::Pinging, this, &MainWindow::SetPinging );
    connect( ui->tableServers->GetServersModel(), &ServersModel::ConnectionError, this, &MainWindow::OnConnectionError );

    connect( ui->tableAllPlayers, &AllPlayersView::SelectedPlayerChanged, ui->tableServers, &ServersView::SetSelectedServer );
    connect( ui->tableAllPlayers, &AllPlayersView::SelectedPlayerChanged, ui->tablePlayers, &PlayersView::SelectPlayerEx );
    connect( ui->tableAllPlayers, &AllPlayersView::OnlinePlayerCountChanged, this, &MainWindow::UpdateOnlinePlayerCountLabel );

    connect( ui->searchPlayer, &QLineEdit::textChanged, ui->tableAllPlayers, &AllPlayersView::SearchPlayer );
    connect( ui->actionRefreshServers, &QAction::triggered, this, &MainWindow::RefreshServers );
    connect( ui->actionConfigure, &QAction::triggered, this, &MainWindow::OpenSettingsDialog );
    connect( ui->actionConnectDisconnect, &QAction::triggered, this, &MainWindow::ConnectOrDisconnect );
    connect( ui->actionAddServer, &QAction::triggered, this, &MainWindow::AddNewServer );
    connect( ui->actionAbout, &QAction::triggered, this, &MainWindow::OpenAboutDialog );
    connect( ui->hideEmpty, &QCheckBox::toggled, ui->tableServers->GetServersProxyModel(), &ServersSortFilterProxyModel::SetHideEmpty );
    connect( ui->hideFull, &QCheckBox::toggled, ui->tableServers->GetServersProxyModel(), &ServersSortFilterProxyModel::SetHideFull );
    connect( ui->hideProxies, &QCheckBox::toggled, ui->tableServers->GetServersProxyModel(), &ServersSortFilterProxyModel::SetHideProxies );

    connect( ui->actionExit, &QAction::triggered, this, &MainWindow::close );

    // Auto connect
    QHostAddress h( settings.value( "servicehost" ).toString() );
    quint16 port = settings.value( "serviceport" ).toUInt();
    if ( !h.isNull() && port ) {
        ConnectToHost( h, port );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::AddNewServer( void ) {
    DialogConnect d( this );
    int r = d.exec();
    if ( r == DialogConnect::Accepted ) {
        ui->tableServers->GetServersModel()->AddServer( d.host(), d.port() );
        QMessageBox::information( this, tr("Add server"), tr("Your server has been submitted for addition.") );
    }
}

void MainWindow::UpdateOnlinePlayerCountLabel( int playerCount ) {
    ui->labelOnlineCount->setText( QString( tr( "%1 players online." ) ).arg( playerCount ) );
}

void MainWindow::RefreshServers( void )
{
    ui->tableServers->RefreshServers();
}

void MainWindow::PingServers( void )
{
    ui->tableServers->PingServers();
}

void MainWindow::SetPinging( bool pinging ) {
    if ( pinging ) {
        work->setText( tr("Pinging servers...") );
    } else {
        if (  connectedFlag ) {
            work->setText( tr("Online") );
        } else {
            work->setText( tr("Offline") );
        }
    }
}

void MainWindow::SetOnline( bool online ) {
    connectedFlag = online;
    if ( connectedFlag ) {
        statusIcon->setPixmap( onlineIcon );
        ui->actionConnectDisconnect->setIcon( disconnectIcon );
        work->setText( tr("Online") );
        ui->actionConnectDisconnect->setToolTip( tr("Disconnect from service") );

        ui->actionAddServer->setEnabled( true );
        ui->actionRefreshServers->setEnabled( true );
    } else {
        statusIcon->setPixmap( offlineIcon );
        ui->actionConnectDisconnect->setIcon( connectIcon );
        work->setText( tr("Offline") );
        ui->actionConnectDisconnect->setToolTip( tr("Connect to service") );
        ui->actionAddServer->setEnabled( false );
        ui->actionRefreshServers->setEnabled( false );
    }
}

void MainWindow::ConnectToHost(const QHostAddress &addr, quint16 port) {
    ui->tableServers->GetServersModel()->Connect( addr, port );
    work->setText( QString( tr("Trying to connect to %1:%2...") ).arg( addr.toString() ).arg( port ) );
}

void MainWindow::ConnectOrDisconnect( void ) {
    if ( !connectedFlag ) {
        DialogConnect d( this );
        int r = d.exec();
        if ( r == DialogConnect::Accepted ) {
            settings.setValue( "servicehost", d.host().toString() );
            settings.setValue( "serviceport", d.port() );
            ConnectToHost( d.host(), d.port() );
        }
    } else {
        ui->tableServers->GetServersModel()->Disconnect();
    }
}

void MainWindow::OnConnectionError( const QString &errorString ) {
    QMessageBox::critical( this, tr("Connection Error"), errorString );
}

void MainWindow::OpenSettingsDialog( void ) {
    DialogSettings d( this );
    d.exec();
}

void MainWindow::OpenAboutDialog( void ) {
    DialogAbout d( this );
    d.exec();
}

