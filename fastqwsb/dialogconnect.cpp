/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "dialogconnect.h"
#include "ui_dialogconnect.h"
#include <QHostAddress>
#include <QHostInfo>
#include <QMessageBox>
#include <QSettings>

DialogConnect::DialogConnect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogConnect)
{
    ui->setupUi(this);
    ui->port->setValidator( new QIntValidator( 1, 256*256, this ) );

    QSettings s;
    ui->host->setText( s.value( "servicehost" ).toString() );
    ui->port->setText( s.value( "serviceport" ).toString() );
}

const QHostAddress& DialogConnect::host() const {
    return hostaddress;
}

quint16 DialogConnect::port() const {
    return ui->port->text().toUShort();
}

void DialogConnect::accept() {
    QHostInfo info = QHostInfo::fromName( ui->host->text() );
    if ( ui->host->text().isEmpty() || ui->port->text().isEmpty() ) {
        QMessageBox::critical( this, tr("Error"), tr("Please specify the service address first.") );
        return;
    }
    int err = info.error();
    if ( err ) {
        if ( err == QHostInfo::HostNotFound ) {
            QMessageBox::critical( this, tr("Error"), tr("Host not found.") );
        } else {
            QMessageBox::critical( this, tr("Error"), tr("An unknown error occured.") );
        }
        QDialog::reject();
    } else {
        hostaddress = QHostAddress( info.addresses().first() );
        QDialog::accept();
    }
}

DialogConnect::~DialogConnect()
{
    delete ui;
}
