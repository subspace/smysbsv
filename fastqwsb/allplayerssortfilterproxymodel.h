/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef ALLPLAYERSSORTFILTERPROXYMODEL_H
#define ALLPLAYERSSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

class AllPlayersModel;
class AllPlayersSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit AllPlayersSortFilterProxyModel(QObject *parent = 0);
    bool filterAcceptsRow( int source_row, const QModelIndex &source_parent ) const;
private:
    AllPlayersModel* model;
};

#endif // ALLPLAYERSSORTFILTERPROXYMODEL_H
