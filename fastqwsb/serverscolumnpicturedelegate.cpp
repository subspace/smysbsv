/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serverscolumnpicturedelegate.h"
#include "serversmodel.h"
#include <QPainter>

ServersColumnPictureDelegate::ServersColumnPictureDelegate(const QMap<QString, QPixmap>& flags, QObject *parent) :
    QAbstractItemDelegate(parent),
    needpass( ":/icons/png/22x22/actions/document-encrypt.png" ),
    dontNeedpass( ":/icons/png/22x22/actions/document-decrypt.png" ),
    flags( flags )
    {

}

void ServersColumnPictureDelegate::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const {
    if ( !index.isValid() ) {
        return;
    }

    if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
        QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                                  ? QPalette::Normal : QPalette::Disabled;
        if (cg == QPalette::Normal && !(option.state & QStyle::State_Active))
            cg = QPalette::Inactive;

        painter->fillRect(option.rect, option.palette.brush(cg, QPalette::Highlight));
    } else {
        painter->fillRect( option.rect, index.data( Qt::BackgroundRole ).value<QBrush>() ) ;
    }

    if ( index.column() == ServersModel::CountryColumn ) {
        QPixmap flag = flags[ index.data( ServersModel::SortRole ).toString() ];
        int x = option.rect.x() + option.rect.width()/2 - flag.width()/2;
        int y = option.rect.y() + option.rect.height()/2 - flag.height()/2;
        painter->drawPixmap( x, y, flag.width(), flag.height(), flag );

        return;
    }

    if ( index.column() == ServersModel::NeedPassColumn ) {
        const QPixmap& pic = index.data( ServersModel::SortRole ).toBool() ? needpass : dontNeedpass;

        int x = option.rect.x() + option.rect.width()/2 - pic.width()/2;
        int y = option.rect.y() + option.rect.height()/2 - pic.height()/2;
        painter->drawPixmap( x, y, pic.width(), pic.height(), pic );

        return;
    }
}

QSize ServersColumnPictureDelegate::sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const {
    QVariant var = index.data( Qt::SizeHintRole );
    if ( var.isValid() ) {
        return var.toSize();
    }
    if ( index.column() == ServersModel::CountryColumn ) {
        return flags[ index.data( ServersModel::SortRole ).toString() ].size();
    }
    if ( index.column() == ServersModel::NeedPassColumn ) {
        const QPixmap& pic = index.data( ServersModel::SortRole ).toBool() ? needpass : dontNeedpass;
        return pic.size();
    }
    return QSize( option.rect.width(), option.rect.height() );
}
