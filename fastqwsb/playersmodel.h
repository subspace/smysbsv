/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef PLAYERSMODEL_H
#define PLAYERSMODEL_H

#include <QAbstractItemModel>
#include "serverinfo.h"

class PlayersModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum {
        PingColumn,
        TimeColumn,
        FragsColumn,
        TeamColumn,
        NameColumn,
        SkinColumn,
        LastColumn
    };

    enum {
        DataRole = Qt::UserRole+1
    };
    explicit PlayersModel(QObject *parent = 0);
    void SetNewPlayerList( const QList<Player>& list );

    QVariant headerData( int section, Qt::Orientation orientation, int role ) const;
    QVariant data( const QModelIndex &index, int role ) const;
    QModelIndex index( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    QModelIndex parent( const QModelIndex &child = QModelIndex() ) const;
    int rowCount( const QModelIndex &parent = QModelIndex() ) const;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QModelIndex PlayerIndexByNumber( int playerNumber ) const;

signals:
    void SelectPlayer( int index );

private:
    QList<Player> players;
    static QVector<int> GetModifiedColumns( const Player& old_player, const Player& new_player );
};

#endif // PLAYERSMODEL_H
