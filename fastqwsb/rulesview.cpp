/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "rulesview.h"
#include "rulesmodel.h"
#include <QHeaderView>
#include <QSortFilterProxyModel>
#include <QEvent>

RulesView::RulesView(QWidget *parent) :
    QTableView(parent),
    proxyModel( new QSortFilterProxyModel( this ) )
{
    setSortingEnabled( true );
    horizontalHeader()->setStretchLastSection( true );
    horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
    verticalHeader()->hide();

    setSelectionBehavior( QAbstractItemView::SelectRows );
    setSelectionMode( QAbstractItemView::SingleSelection );

    proxyModel->sort( 0, Qt::AscendingOrder );
    horizontalHeader()->setSortIndicator( 0, Qt::AscendingOrder );

    QTableView::setModel( proxyModel );
}

void RulesView::setModel( QAbstractItemModel *model ) {
    proxyModel->setSourceModel( model );
    this->model = qobject_cast<RulesModel*>( model );
}


bool RulesView::viewportEvent(QEvent *event) {
    if ( event->type() == QEvent::Paint ) {
        resizeColumnToContents( 0 );
    }
    return QTableView::viewportEvent( event );
}
