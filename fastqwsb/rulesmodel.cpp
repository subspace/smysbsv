/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "rulesmodel.h"

RulesModel::RulesModel(QObject *parent) :
    QAbstractItemModel(parent)
{
}

void RulesModel::SetNewRuleList( const QList<Rule> &list ) {
    if ( rules.size() ) {
        beginRemoveRows( QModelIndex(), 0, rules.size()-1 );
        rules.clear();
        endRemoveRows();
    }

    if ( list.size() ) {
        beginInsertRows( QModelIndex(), 0, list.size()-1 );
        rules = list;
        endInsertRows();
    }
}

QVariant RulesModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( role != Qt::DisplayRole || orientation != Qt::Horizontal ) {
        return QAbstractItemModel::headerData( section, orientation, role );
    }
    switch ( section ) {
    case RuleColumn:
        return QVariant( tr( "Rule" ) );
    case ValueColumn:
        return QVariant( tr( "Value" ) );
    }
    return QAbstractItemModel::headerData( section, orientation, role );
}

QVariant RulesModel::data( const QModelIndex &index, int role ) const {
    if ( role != Qt::DisplayRole || !index.isValid() ) {
        return QVariant();
    }

    int row = index.row();
    int column = index.column();

    if ( row > rules.size() ) {
        return QVariant();
    }

    const Rule& rule = rules.at( row );
    switch ( column ) {
    case RuleColumn:
        return QVariant( rule.name );
    case ValueColumn:
        return QVariant( rule.value );
    }
    return QVariant();
}

QModelIndex RulesModel::index( int row, int column, const QModelIndex & ) const {
    return createIndex( row, column );
}

QModelIndex RulesModel::parent( const QModelIndex & ) const {
    return QModelIndex();
}

int RulesModel::rowCount( const QModelIndex & ) const {
    return rules.size();
}

int RulesModel::columnCount( const QModelIndex & ) const {
    return LastColumn;
}
