/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "dialogsettings.h"
#include "ui_dialogsettings.h"
#include <QStyleFactory>
#include <QApplication>
#include <QFileDialog>
#include <QStringListModel>
#include <QDebug>

DialogSettings::DialogSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSettings)
{
    ui->setupUi(this);
    connect( this, &DialogSettings::accepted, this, &DialogSettings::SaveSettings );
    QStringListModel* stylesModel = new QStringListModel( this );
    ui->styles->setModel( stylesModel );
    stylesModel->setStringList( QStyleFactory::keys() );
    ui->styles->setEditable( false );

    ui->styles->setCurrentText( settings.value( "appstyle" ).toString() );
    ui->qwPath->setText( settings.value( "qwpath" ).toString() );
    ui->qwExtraCmdline->setText( settings.value( "qwcmdline" ).toString() );
}

DialogSettings::~DialogSettings()
{
    delete ui;
}

void DialogSettings::on_qwBrowse_clicked()
{
    QString qwpath = QFileDialog::getOpenFileName( this, tr("Select your QW binary") );

    if ( qwpath.isEmpty() ) {
        return;
    }
    qDebug() << qwpath;
    ui->qwPath->setText( qwpath );
}

void DialogSettings::SaveSettings( void ) {
    settings.setValue( "qwpath", ui->qwPath->text() );
    settings.setValue( "qwcmdline", ui->qwExtraCmdline->text() );
    settings.setValue( "appstyle", ui->styles->currentText() );
    qApp->setStyle( QStyleFactory::create( ui->styles->currentText() ) );
}
