/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serverssortfilterproxymodel.h"
#include "serversmodel.h"
#include <QSortFilterProxyModel>
#include <QDebug>

ServersSortFilterProxyModel::ServersSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    model( new ServersModel( this ) ),
    hideProxies( false ),
    hideFull( false ),
    hideEmpty( false )
{
    setSourceModel( model );
    setDynamicSortFilter( true );
    setSortRole( ServersModel::SortRole );
}

void ServersSortFilterProxyModel::sort(int column, Qt::SortOrder order) {
    if ( column != ServersModel::PingColumn ) { // Show the lowest ping servers first
        QSortFilterProxyModel::sort( ServersModel::PingColumn, Qt::AscendingOrder );
    } else { // Show the servers with most players first
        QSortFilterProxyModel::sort( ServersModel::PlayersColumn, Qt::DescendingOrder );
    }
    QSortFilterProxyModel::sort( column, order );
}

bool ServersSortFilterProxyModel::filterAcceptsRow( int source_row, const QModelIndex &source_parent ) const {
    if ( hideEmpty && model->IsEmpty( source_row ) ) {
        return false;
    }
    if ( hideFull && model->IsFull( source_row ) ) {
        return false;
    }
    if ( hideProxies && model->IsProxy( source_row ) ) {
        return false;
    }
    return QSortFilterProxyModel::filterAcceptsRow( source_row, source_parent );
}
