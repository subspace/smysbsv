/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "playersview.h"
#include "playersmodel.h"
#include "playersfragscolumndelegate.h"
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include <QTimer>

PlayersView::PlayersView( QWidget *parent ) :
    QTableView( parent ),
    proxyModel( new QSortFilterProxyModel( this ) )
{
    setSortingEnabled( true );
    horizontalHeader()->setSectionsMovable( true );
    horizontalHeader()->setStretchLastSection( true );
    horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );

    verticalHeader()->hide();

    setSelectionBehavior( QAbstractItemView::SelectRows );
    setSelectionMode( QAbstractItemView::SingleSelection );

    proxyModel->sort( 2, Qt::DescendingOrder );
    horizontalHeader()->setSortIndicator( 2, Qt::DescendingOrder );
    QTableView::setModel( proxyModel );
}

bool PlayersView::viewportEvent(QEvent *event) { // Hack to fix little bug on ResizeToContents flag;
    if ( event->type() == QEvent::Paint ) {
        resizeColumnToContents( PlayersModel::NameColumn );
    }
    return QTableView::viewportEvent( event );
}

void PlayersView::setModel( QAbstractItemModel *model ) {
    proxyModel->setSourceModel( model );
    this->model = qobject_cast<PlayersModel*>( model );
    PlayersFragsColumnDelegate* pd = new PlayersFragsColumnDelegate( model );
    setItemDelegateForColumn( PlayersModel::FragsColumn, pd );
}

void PlayersView::SelectPlayer( int number ) {
    selectionModel()->setCurrentIndex( proxyModel->mapFromSource( model->PlayerIndexByNumber( number ) ), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows );
}

void PlayersView::SelectPlayerEx(int , const Player &player) {
    SelectPlayer( player.number );
}
