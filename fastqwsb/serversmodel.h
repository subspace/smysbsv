/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERSMODEL_H
#define SERVERSMODEL_H

#include <QAbstractItemModel>
#include <QPixmap>
#include "serverinfo.h"
#include "pingsocket.h"

class ClientSocket;

class ServersModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum ColumnIndexes {
        CountryColumn,
        PingColumn,
        PlayersColumn,
        SpectatorsColumn,
        MapColumn,
        NeedPassColumn,
        AddressColumn,
        NameColumn,
        LastColumn
    };

    enum {
        SortRole = Qt::UserRole+1
    };

    explicit ServersModel(QObject *parent = 0);
    void Connect( const QHostAddress& address, quint16 port );
    void Disconnect( void );
    void PingServers( void );
    void PingServer( const QHostAddress& address, quint16 port );
    void RefreshServers( void );
    void RefreshServer( const QModelIndex& idx );
    void AddServer( const QHostAddress& addr, quint16 port );
    bool IsProxy( int index ) const;
    bool IsFull( int index ) const;
    bool IsEmpty( int index ) const;

signals:
    void ServerListChanged( const QList<ServerInfo>& list );
    void Connected( bool connected );
    void Pinging( bool pinging );
    void ConnectionError( const QString& error );

public slots:
    void QueryAllServers( void );
    const ServerInfo& ServerInformation( const QModelIndex& index ) const;
    const QList<Player>& Players( const QModelIndex& index ) const;
    const QList<Rule>& Rules( const QModelIndex& index ) const;

public:
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const;
    QVariant data( const QModelIndex &index, int role ) const;
    QModelIndex index( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    QModelIndex parent( const QModelIndex &child ) const;
    int rowCount( const QModelIndex &parent = QModelIndex() ) const;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;

private:
    ClientSocket*       socket;
    PingSocket*         pingSocket;
    QList<ServerInfo>   servers;

    void OnNewServerListArrival( const QList<ServerInfo>& list );
    void OnNewServerInfoArrival( const ServerInfo& info );
    void OnConnection( void );
    void OnDisconnection( void );
    void OnConnectionError( int errorCode );
    void OnPong( const QHostAddress& addr, quint16 port, quint16 ms );
};

#endif // SERVERSMODEL_H
