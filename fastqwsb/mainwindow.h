/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QSettings>
namespace Ui {
class MainWindow;
}

class QLabel;
class QHostAddress;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void RefreshServers( void );
    void PingServers( void );
    void UpdateOnlinePlayerCountLabel( int playerCount );
    void SetOnline( bool online = true );
    void SetPinging( bool pinging = true );
    void OpenSettingsDialog( void );
    void OpenAboutDialog( void );
    void ConnectOrDisconnect( void );
    void OnConnectionError( const QString& errorString );
    void AddNewServer( void );
    void ConnectToHost( const QHostAddress& addr, quint16 port );

private:
    Ui::MainWindow *ui;
    QMap<QString, QPixmap> flags;
    QLabel* statusIcon;
    bool connectedFlag;
    QLabel* work;
    QPixmap onlineIcon;
    QPixmap offlineIcon;
    QPixmap connectIcon;
    QPixmap disconnectIcon;
    QSettings settings;
};

#endif // MAINWINDOW_H
