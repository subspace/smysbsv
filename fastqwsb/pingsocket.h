/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef PINGERSOCKET_H
#define PINGERSOCKET_H

#include <QUdpSocket>
#include <QList>
#include <QDateTime>

class QTimer;
class PingSocket : public QUdpSocket
{
    Q_OBJECT
public:
    explicit PingSocket(QObject *parent = 0);
    void PingServer( const QHostAddress &server , quint16 port );

signals:
    void ResponseTime( const QHostAddress& server, quint16 port, quint16 ms );
    void Pinging( bool pinging );

private:
    enum {
        MaxActivePingRequests = 20,
        MaxWaitingTime = 3000,
        IntervalBetweenRequests = 20, // Against throttling
        PingRequestsForAverage = 1
    };
    struct ActivePing {
        ActivePing( const QHostAddress& address, quint16 port ) :
            addr( address ), port( port ), replyTime( QDateTime() ), numPings( 0 ) {}

        QHostAddress    addr;
        quint16         port;
        QDateTime       requestTime;
        QDateTime       replyTime;
        quint16         pings[PingRequestsForAverage];
        quint8          numPings;

        bool operator ==( const ActivePing& other ) const {
            return ( other.addr == addr && other.port == port );
        }
    };
    QTimer*           timer;
    QList<ActivePing> queued;
    QList<ActivePing> active;

    void CheckTimedoutRequests( void );
    void ParseServerResponse( void );
    void FillFreeSlots( void );
    static quint16 CalculateAveragePing( const quint16 pings[] );
};

#endif // PINGERSOCKET_H
