/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "allplayersview.h"
#include "allplayersmodel.h"
#include "allplayerssortfilterproxymodel.h"
#include <QHeaderView>

AllPlayersView::AllPlayersView(QWidget *parent) :
    QTableView(parent),
    lastPlayerCount( 0 ),
    rowChanged( false )
{
    setSortingEnabled( true );
    horizontalHeader()->setStretchLastSection( true );
    horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    verticalHeader()->hide();
    setSelectionBehavior( QAbstractItemView::SelectRows );
    setSelectionMode( QAbstractItemView::SingleSelection );
}

void AllPlayersView::SearchPlayer( const QString &text ) {
    proxyModel->setFilterWildcard( text );
}

void AllPlayersView::setModel( QAbstractItemModel *model ) {
    this->proxyModel = qobject_cast<AllPlayersSortFilterProxyModel*>( model );
    this->model = qobject_cast<AllPlayersModel*>( proxyModel->sourceModel() );

    proxyModel->sort( AllPlayersModel::CountryColumn );
    horizontalHeader()->setSortIndicator( AllPlayersModel::CountryColumn, Qt::AscendingOrder );

    QTableView::setModel( model );
    connect( selectionModel(), &QItemSelectionModel::currentRowChanged, this, &AllPlayersView::RowChanged );
    connect( this, &AllPlayersView::clicked, this, &AllPlayersView::Clicked );
    connect( this->model, &AllPlayersModel::rowsInserted, this, &AllPlayersView::InnerModelRowCountChanged );
    connect( this->model, &AllPlayersModel::rowsRemoved, this, &AllPlayersView::InnerModelRowCountChanged );
}

void AllPlayersView::Clicked( const QModelIndex &cur ) {
    if ( !cur.isValid() || rowChanged ) {
        rowChanged = false;
        return;
    }
    const AllPlayersModel::PlayerInfo p = model->GetPlayerInfo( proxyModel->mapToSource( cur ) );
    emit SelectedPlayerChanged( p.serverIndex, p.playerData );
}

void AllPlayersView::RowChanged( const QModelIndex &cur, const QModelIndex & ) {
    if ( !cur.isValid() ) {
        return;
    }
    const AllPlayersModel::PlayerInfo p = model->GetPlayerInfo( proxyModel->mapToSource( cur ) );
    emit SelectedPlayerChanged( p.serverIndex, p.playerData );
    rowChanged = true;
}

void AllPlayersView::InnerModelRowCountChanged( const QModelIndex &, int , int )  {
    if ( model->rowCount() != lastPlayerCount ) {
        lastPlayerCount = model->rowCount();
        emit OnlinePlayerCountChanged( lastPlayerCount );
    }
}
