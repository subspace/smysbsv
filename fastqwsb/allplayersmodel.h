/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef ALLPLAYERSMODEL_H
#define ALLPLAYERSMODEL_H

#include <QAbstractItemModel>
#include "serverinfo.h"

class ServerInfo;
class AllPlayersModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum {
        CountryColumn,
        NameColumn,
        LastColumn
    };

    enum {
        SortRole = Qt::UserRole+1
    };

    struct PlayerInfo {
        int serverIndex;
        bool isInAProxy;
        QByteArray countryCode;
        Player playerData;
        bool operator==( const PlayerInfo& other ) const {
            return ( other.serverIndex == this->serverIndex && other.playerData == this->playerData );
        }
    };

    explicit AllPlayersModel(QObject *parent = 0);
    void SetData( const QList<ServerInfo> &list );
    const PlayerInfo& GetPlayerInfo( const QModelIndex& index ) const;

    QVariant headerData( int section, Qt::Orientation orientation, int role ) const;
    QVariant data( const QModelIndex &index, int role ) const;
    QModelIndex index( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    QModelIndex parent( const QModelIndex &child = QModelIndex() ) const;
    int rowCount( const QModelIndex &parent = QModelIndex() ) const;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;

private:
    QList<PlayerInfo> players;
};

#endif // ALLPLAYERSMODEL_H
