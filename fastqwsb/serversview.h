/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERSVIEW_H
#define SERVERSVIEW_H

#include <QTableView>
#include "serverinfo.h"

class ServersModel;
class ServersSortFilterProxyModel;
class PlayersModel;
class RulesModel;
class AllPlayersSortFilterProxyModel;
class Player;
class QMenu;
class ServersView : public QTableView
{
    Q_OBJECT
public:
    explicit ServersView( QWidget *parent = 0 );

    ServersModel*    GetServersModel( void ) const;
    ServersSortFilterProxyModel* GetServersProxyModel( void ) const;
    PlayersModel*    GetPlayersModel( void ) const;
    RulesModel*      GetRulesModel( void ) const;
    AllPlayersSortFilterProxyModel* GetAllPlayersModel( void ) const;
    void             RefreshServers( void );
    void             PingServers( void );
    void             SetSelectedServer( int index, const Player& selected );
    virtual bool     viewportEvent( QEvent *event );

protected:
    virtual void contextMenuEvent( QContextMenuEvent *e );

private:
    ServersModel*   model;
    ServersSortFilterProxyModel* proxyModel;
    PlayersModel*   playersModel;
    RulesModel*     rulesModel;
    AllPlayersSortFilterProxyModel*allPlayersModel;
    QAction*        joinServer;
    QAction*        joinSpectator;
    QAction*        joinQTV;
    QAction*        refreshAll;
    QMenu*          contextMenu;
    QMenu*          qtvSubMenu;
    ServerAddress   lastSelectedServer;

    void RowChanged( const QModelIndex& current, const QModelIndex& previous );
    void JoinSelected( void );
    void ObserveSelected( void );
    void ObserveSelectedByQTV( void );
    QModelIndex FindServerIndex( const ServerAddress& server );
    void OnServerListChange( const QList<ServerInfo>& list );
};

#endif // SERVERSVIEW_H
