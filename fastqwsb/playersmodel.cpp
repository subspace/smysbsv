/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "playersmodel.h"
#include "utils.h"

PlayersModel::PlayersModel(QObject *parent) :
    QAbstractItemModel(parent)
{
}

QVector<int> PlayersModel::GetModifiedColumns( const Player& old_player, const Player& new_player ) { // Only for visible columns
    QVector<int> modified_columns;
    if ( old_player.ping != new_player.ping ) {
        modified_columns.push_back( PingColumn );
    }
    if ( old_player.time != new_player.time ) {
        modified_columns.push_back( TimeColumn );
    }
    if ( old_player.score != new_player.score ) {
        modified_columns.push_back( FragsColumn );
    }
    if ( old_player.team != new_player.team ) {
        modified_columns.push_back( TeamColumn );
    }
    if ( old_player.name != new_player.name ) {
        modified_columns.push_back( NameColumn );
    }
    if ( old_player.skin != new_player.skin ) {
        modified_columns.push_back( SkinColumn );
    }
    return modified_columns;
}

void PlayersModel::SetNewPlayerList( const QList<Player> &list ) {
    if ( players.size() ) {
        beginRemoveRows( QModelIndex(), 0, players.size()-1 );
        players.clear();
        endRemoveRows();
    }

    if ( list.size() ) {
        beginInsertRows( QModelIndex(), 0, list.size()-1 );
        players = list;
        endInsertRows();
    }
}

QModelIndex PlayersModel::PlayerIndexByNumber( int playerNumber ) const {
    for ( int i = 0; i < players.size(); ++i ) {
        if ( players.at( i ).number == playerNumber ) {
            return index( i, 0 );
        }
    }
    return QModelIndex();
}

QVariant PlayersModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( role != Qt::DisplayRole || orientation != Qt::Horizontal ) {
        return QAbstractItemModel::headerData( section, orientation, role );
    }
    switch ( section ) {
    case PingColumn:
        return QVariant( tr( "Ping" ) );
    case TimeColumn:
        return QVariant( tr( "Time" ) );
    case FragsColumn:
        return QVariant( tr( "Frags" ) );
    case TeamColumn:
        return QVariant( tr( "Team" ) );
    case NameColumn:
        return QVariant( tr( "Name" ) );
    case SkinColumn:
        return QVariant( tr( "Skin" ) );
    }
    return QAbstractItemModel::headerData( section, orientation, role );
}

#include <QSize>
QVariant PlayersModel::data( const QModelIndex &index, int role ) const {
    if ( !index.isValid() ) {
        return QVariant();
    }

    int row = index.row();
    int column = index.column();

    if ( row > players.size() ) {
        return QVariant();
    }

    if ( role == Qt::SizeHintRole ) {
//        return QSize(200,200);
    }
    if ( role == Qt::DisplayRole ) {
        const Player& player = players.at( row );
        switch ( column ) {
        case PingColumn:
            return QVariant( player.ping );
        case TimeColumn:
            return QVariant( player.time );
        case FragsColumn:
            return QVariant( player.score );
        case TeamColumn:
            return QVariant( player.team );
        case NameColumn:
            return QVariant( player.name );
        case SkinColumn:
            return QVariant( player.skin );
        }
    }

    if ( role == DataRole ) {
        return qVariantFromValue((void*)&players.at( row ) );
    }
    return QVariant();
}

QModelIndex PlayersModel::index( int row, int column, const QModelIndex & ) const {
    QModelIndex idx = createIndex( row, column );
    return idx;
}

QModelIndex PlayersModel::parent( const QModelIndex & ) const {
    return QModelIndex();
}

int PlayersModel::rowCount( const QModelIndex & ) const {
    return players.size();
}

int PlayersModel::columnCount( const QModelIndex & ) const {
    return LastColumn;
}
