/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERSSORTFILTERPROXYMODEL_H
#define SERVERSSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

class ServersModel;
class ServersSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ServersSortFilterProxyModel(QObject *parent = 0);
    void SetHideFull( bool enable ) { hideFull = enable; invalidateFilter(); }
    void SetHideEmpty( bool enable ) { hideEmpty = enable; invalidateFilter(); }
    void SetHideProxies( bool enable ) { hideProxies = enable; invalidateFilter(); }
    virtual void sort( int column, Qt::SortOrder order );
    virtual bool filterAcceptsRow( int source_row, const QModelIndex &source_parent ) const;
private:
    ServersModel* model;
    bool hideProxies;
    bool hideFull;
    bool hideEmpty;
};

#endif // SERVERSSORTFILTERPROXYMODEL_H
