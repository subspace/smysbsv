/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "allplayerssortfilterproxymodel.h"
#include "allplayersmodel.h"
AllPlayersSortFilterProxyModel::AllPlayersSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    model( new AllPlayersModel( this ) )
{
    setSourceModel( model );
    setSortRole( AllPlayersModel::SortRole );
    sort( AllPlayersModel::NameColumn );
    setFilterKeyColumn( AllPlayersModel::NameColumn );
    setFilterCaseSensitivity( Qt::CaseInsensitive );
}

bool AllPlayersSortFilterProxyModel::filterAcceptsRow( int source_row, const QModelIndex &source_parent ) const {
    return QSortFilterProxyModel::filterAcceptsRow( source_row, source_parent );
}
