/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef PLAYERSVIEW_H
#define PLAYERSVIEW_H

#include <QTableView>
#include <QEvent>
#include <QDebug>
class PlayersModel;
class QSortFilterProxyModel;
class Player;
class PlayersView : public QTableView
{
    Q_OBJECT
public:
    explicit PlayersView( QWidget *parent = 0 );
    virtual void setModel( QAbstractItemModel *model );

    void SelectPlayer( int number );
    void SelectPlayerEx( int server_index, const Player& player );
    virtual bool viewportEvent( QEvent *event );

private:
    PlayersModel* model;
    QSortFilterProxyModel* proxyModel;
};

#endif // PLAYERSVIEW_H
