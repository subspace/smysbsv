#-------------------------------------------------
#
# Project created by QtCreator 2013-10-15T19:12:49
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fastqwsb
TEMPLATE = app

INCLUDEPATH += ../
SOURCES += main.cpp\
        mainwindow.cpp \
    serversview.cpp \
    playersview.cpp \
    rulesview.cpp \
    clientsocket.cpp \
    ../serverinfo.cpp \
    serversmodel.cpp \
    playersmodel.cpp \
    rulesmodel.cpp \
    pingsocket.cpp \
    serverssortfilterproxymodel.cpp \
    utils.cpp \
    allplayersmodel.cpp \
    allplayersview.cpp \
    allplayerssortfilterproxymodel.cpp \
    serverscolumnpicturedelegate.cpp \
    playersfragscolumndelegate.cpp \
    allplayerscountrydelegate.cpp \
    dialogsettings.cpp \
    dialogconnect.cpp \
    dialogabout.cpp

HEADERS  += mainwindow.h \
    serversview.h \
    playersview.h \
    rulesview.h \
    clientsocket.h \
    serversmodel.h \
    playersmodel.h \
    rulesmodel.h \
    pingsocket.h \
    serverssortfilterproxymodel.h \
    utils.h \
    allplayersmodel.h \
    allplayersview.h \
    allplayerssortfilterproxymodel.h \
    serverscolumnpicturedelegate.h \
    playersfragscolumndelegate.h \
    allplayerscountrydelegate.h \
    ../serverinfo.h \
    dialogsettings.h \
    dialogconnect.h \
    dialogabout.h

FORMS    += mainwindow.ui \
    dialogsettings.ui \
    dialogconnect.ui \
    dialogabout.ui

RESOURCES += \
    resources.qrc
