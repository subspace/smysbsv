/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "allplayersmodel.h"
#include "serverinfo.h"
#include "utils.h"

AllPlayersModel::AllPlayersModel(QObject *parent) :
    QAbstractItemModel(parent)
{
}

void AllPlayersModel::SetData( const QList<ServerInfo>& list ) {
    if ( players.size() ) {
        beginRemoveRows( QModelIndex(), 0, players.size()-1 );
        players.clear();
        endRemoveRows();
    }

    QList<PlayerInfo> plist;
    int playerCount = 0;
    for ( int i = 0; i < list.size(); ++i ) {
        const ServerInfo& info = list.at( i );
        for ( int j = 0; j < info.players.size(); ++j ) {
            const Player& player = info.players.at( j );
            PlayerInfo newPlayer;
            newPlayer.serverIndex = i;
            newPlayer.countryCode = info.countryCode;
            newPlayer.playerData = player;
            newPlayer.isInAProxy = info.isProxy;
            plist.append( newPlayer );
        }
        playerCount += info.players.size();
    }

    if ( playerCount ) {
        beginInsertRows( QModelIndex(), 0, playerCount-1 );
        players = plist;
        endInsertRows();
    }
}

const AllPlayersModel::PlayerInfo& AllPlayersModel::GetPlayerInfo( const QModelIndex &index ) const {
    return players.at( index.row() );
}

QVariant AllPlayersModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( role != Qt::DisplayRole || orientation != Qt::Horizontal ) {
        return QAbstractItemModel::headerData( section, orientation, role );
    }
    switch ( section ) {
    case CountryColumn:
        return QVariant( tr( "Country" ) );
    case NameColumn:
        return QVariant( tr( "Name" ) );
    }
    return QAbstractItemModel::headerData( section, orientation, role );
}

QVariant AllPlayersModel::data( const QModelIndex &index, int role ) const {
    if ( !index.isValid() ) {
        return QVariant();
    }
    int row = index.row();
    int column = index.column();

    if ( row > players.size() ) {
        return QVariant();
    }
    const PlayerInfo& player = players.at( row );
    if ( role == Qt::DisplayRole || role == SortRole ) {
        switch ( column ) {
        case CountryColumn:
            return QVariant( player.countryCode );
        case NameColumn:
            return QVariant( player.playerData.name );
        }
    }

    return QVariant();
}

QModelIndex AllPlayersModel::index( int row, int column, const QModelIndex & ) const {
    return createIndex( row, column );
}

QModelIndex AllPlayersModel::parent( const QModelIndex & ) const {
    return QModelIndex();
}

int AllPlayersModel::rowCount( const QModelIndex & ) const {
    return players.size();
}

int AllPlayersModel::columnCount( const QModelIndex & ) const {
    return LastColumn;
}
