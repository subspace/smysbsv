/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serversview.h"
#include "serversmodel.h"
#include "playersmodel.h"
#include "allplayersmodel.h"
#include "allplayerssortfilterproxymodel.h"
#include "rulesmodel.h"
#include "serverssortfilterproxymodel.h"
#include "serverscolumnpicturedelegate.h"
#include <QHeaderView>
#include <QEvent>
#include <QContextMenuEvent>
#include <QMenu>
#include <QProcess>
#include <QSettings>
#include <QToolButton>

ServersView::ServersView(QWidget *parent) :
    QTableView(parent),
    proxyModel( new ServersSortFilterProxyModel( this ) ),
    playersModel( new PlayersModel( this ) ),
    rulesModel( new RulesModel( this ) ),
    allPlayersModel( new AllPlayersSortFilterProxyModel( this ) ),
    joinServer( new QAction( QIcon( ":/icons/png/22x22/actions/fork.png" ), tr("Join"), this ) ),
    joinSpectator( new QAction( QIcon( ":/icons/png/22x22/actions/edit-find-7.png" ), tr("Observe"), this ) ),
    refreshAll( new QAction( QIcon( ":/icons/png/22x22/actions/db_update.png" ), tr("Refresh all servers"), this ) ),
    contextMenu( new QMenu( this ) ),
    qtvSubMenu( new QMenu( tr("Observe with QTV"), contextMenu ) )
{
    setSortingEnabled( true );
    setContextMenuPolicy( Qt::DefaultContextMenu );
    proxyModel->sort( ServersModel::PlayersColumn, Qt::DescendingOrder );
    horizontalHeader()->setSortIndicator( ServersModel::PlayersColumn, Qt::DescendingOrder );

    setModel( proxyModel );
    model = qobject_cast<ServersModel*>( proxyModel->sourceModel() );

    horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
    horizontalHeader()->setSectionsMovable( true );
    verticalHeader()->hide();

    connect( selectionModel(), &QItemSelectionModel::currentRowChanged, this, &ServersView::RowChanged );
    connect( model, &ServersModel::ServerListChanged, qobject_cast<AllPlayersModel*>( allPlayersModel->sourceModel() ), &AllPlayersModel::SetData );
    connect( model, &ServersModel::ServerListChanged, this, &ServersView::OnServerListChange );

    contextMenu->addAction( joinServer );
    contextMenu->addAction( joinSpectator );
    qtvSubMenu->setIcon( QIcon( ":/icons/png/22x22/actions/edit-find-7.png" ) );
    contextMenu->addMenu( qtvSubMenu );
    contextMenu->addSeparator();
    contextMenu->addAction( refreshAll );
    connect( joinServer, &QAction::triggered, this, &ServersView::JoinSelected );
    connect( joinSpectator, &QAction::triggered, this, &ServersView::ObserveSelected );
    connect( refreshAll, &QAction::triggered, this, &ServersView::RefreshServers );
}

ServersSortFilterProxyModel* ServersView::GetServersProxyModel( void ) const {
    return proxyModel;
}

bool ServersView::viewportEvent( QEvent *event ) {
    if ( event->type() == QEvent::Paint ) {
        resizeColumnToContents( ServersModel::AddressColumn );
    }
    return QTableView::viewportEvent( event );
}

void ServersView::SetSelectedServer( int idx, const Player & ) {
    selectionModel()->setCurrentIndex( proxyModel->mapFromSource( model->index( idx, 0 ) ), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows );// QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows );
}

QModelIndex ServersView::FindServerIndex( const ServerAddress &server ) {
    for ( int i = 0; i < model->rowCount(); ++i ) {
        QModelIndex idx = model->index( i, 0 );
        const ServerInfo& info = model->ServerInformation( idx );
        if ( info.host == server ) {
            return proxyModel->mapFromSource( idx );
        }
    }
    return QModelIndex();
}

void ServersView::OnServerListChange( const QList<ServerInfo>&  ) {
    QModelIndex idx = FindServerIndex( lastSelectedServer );
    selectionModel()->setCurrentIndex( idx, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows );
    RowChanged( idx, idx );
    //model->PingServers();
}

void ServersView::RefreshServers( void ) {
    QModelIndex idx = proxyModel->mapToSource( selectionModel()->currentIndex() );
    if ( idx.isValid() ) {
        lastSelectedServer = model->ServerInformation( idx ).host;
    }
    model->RefreshServers();
}

void ServersView::PingServers( void ) {
    //model->PingServers();
}

PlayersModel* ServersView::GetPlayersModel( void ) const {
    return playersModel;
}

RulesModel* ServersView::GetRulesModel( void ) const {
    return rulesModel;
}

ServersModel* ServersView::GetServersModel( void ) const {
    return model;
}

AllPlayersSortFilterProxyModel* ServersView::GetAllPlayersModel( void ) const {
    return allPlayersModel;
}

void ServersView::RowChanged( const QModelIndex &current, const QModelIndex & ) {
    QModelIndex idx = proxyModel->mapToSource( current );
    if ( !idx.isValid() ) {
        playersModel->SetNewPlayerList( QList<Player>() );
        rulesModel->SetNewRuleList( QList<Rule>() );
        return;
    }

    const ServerInfo& info = model->ServerInformation( idx );
    playersModel->SetNewPlayerList( info.players );
    rulesModel->SetNewRuleList( info.rules );
}

void ServersView::contextMenuEvent( QContextMenuEvent *e ) {
    QModelIndex idx = proxyModel->mapToSource( selectionModel()->currentIndex() );
    if ( !idx.isValid() ) {
        return;
    }

    const ServerInfo& info = model->ServerInformation( idx );
    contextMenu->setTitle( info.hostname );
    if ( info.qtvStreams.isEmpty() ) {
        qtvSubMenu->setEnabled( false );
        QList<QAction*> for_removal;
        for ( int i = 0; i < qtvSubMenu->actions().size(); ++i ) {
            for_removal.push_back( qtvSubMenu->actions().at( i ) );
        }
        for ( int i = 0; i < for_removal.size(); ++i ) {
            qtvSubMenu->removeAction( for_removal.at( i ) );
            for_removal.at( i )->deleteLater();
        }
    } else {
        qtvSubMenu->setEnabled( true );
        QList<QAction*> for_removal;
        for ( int i = 0; i < qtvSubMenu->actions().size(); ++i ) {
            for_removal.push_back( qtvSubMenu->actions().at( i ) );
        }
        for ( int i = 0; i < for_removal.size(); ++i ) {
            qtvSubMenu->removeAction( for_removal.at( i ) );
            for_removal.at( i )->deleteLater();
        }
        for ( int i = 0; i < info.qtvStreams.size(); ++i ) {
            const QTVStream& s = info.qtvStreams.at( i );
            QAction* a = qtvSubMenu->addAction( joinSpectator->icon(), QString("%1 (%2@%3:%4)").arg( s.host.name ).arg( s.streamNumber ).arg( s.host.address.toString() ).arg( s.host.port ) );
            connect( a, &QAction::triggered, this, &ServersView::ObserveSelectedByQTV );
        }
    }
    contextMenu->exec( e->globalPos() );
}

void ServersView::JoinSelected( void ) {
    QModelIndex index = proxyModel->mapToSource( selectionModel()->currentIndex() );
    if ( !index.isValid() ) {
        return;
    }

    const ServerInfo& info = model->ServerInformation( index );
    QSettings s;

    QProcess::startDetached(
                s.value( "qwpath" ).toString(),
                QStringList() << "+spectator 0"
                              << QString("+connect %1:%2").arg( info.host.address.toString() ).arg( info.host.port )
                              << s.value( "qwcmdline" ).toString(),
                s.value( "qwpath" ).toString().replace( QRegExp( "[^\\\\//]+$" ), "" )
                );
}

void ServersView::ObserveSelected( void ) {
    QModelIndex index = proxyModel->mapToSource( selectionModel()->currentIndex() );
    if ( !index.isValid() ) {
        return;
    }

    const ServerInfo& info = model->ServerInformation( index );
    QSettings s;
    QProcess::startDetached(
                s.value( "qwpath" ).toString(),
                QStringList() << "+spectator 1"
                              << QString("+connect %1:%2").arg( info.host.address.toString() ).arg( info.host.port )
                              << s.value( "qwcmdline" ).toString(),
                s.value( "qwpath" ).toString().replace( QRegExp( "[^\\\\//]+$" ), "" )
                );
}

void ServersView::ObserveSelectedByQTV( void ) {
    QModelIndex index = proxyModel->mapToSource( selectionModel()->currentIndex() );
    if ( !index.isValid() ) {
        return;
    }

    QAction* action = qobject_cast<QAction*>( sender() );
    QSettings s;
    QProcess::startDetached(
                s.value( "qwpath" ).toString(),
                QStringList() << QString("+qtvplay %1").arg( action->text() )
                              << s.value( "qwcmdline" ).toString(),
                s.value( "qwpath" ).toString().replace( QRegExp( "[^\\\\//]+$" ), "" )
                );
}
