/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "playersfragscolumndelegate.h"
#include "playersmodel.h"
#include "serverinfo.h"
#include <QPainter>
#include <qmath.h>

PlayersFragsColumnDelegate::PlayersFragsColumnDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
}

void PlayersFragsColumnDelegate::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const {
    if ( !index.isValid() ) {
        return;
    }

    if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
        QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                                  ? QPalette::Normal : QPalette::Disabled;
        if (cg == QPalette::Normal && !(option.state & QStyle::State_Active))
            cg = QPalette::Inactive;

        painter->fillRect(option.rect, option.palette.brush(cg, QPalette::Highlight));
    } else {
        painter->fillRect( option.rect, index.data( Qt::BackgroundRole ).value<QBrush>() ) ;
    }
    if ( index.column() != PlayersModel::FragsColumn )
        return;

    const Player* player = static_cast<Player*>( index.data( PlayersModel::DataRole ).value<void*>() );
    int w = option.rect.width()-4;
    int h = option.rect.height()/2-2;
    int x = option.rect.x()+2;
    int y = option.rect.y()+2;

    QColor shirt( player->shirtColor );
    QColor pants( player->pantsColor );
    shirt.setAlphaF(0.3f);
    pants.setAlphaF(0.3f);
    painter->fillRect( x, y, w, h, shirt );
    painter->fillRect( x, y+h, w, h, pants );
    painter->drawRect( x, y, w-1, h*2-1 );
    painter->save();

    QFont f = painter->font();
    f.setBold( true );
    QRect r = option.rect.adjusted(2, 2, -2, -2);
    painter->setFont( f );
    painter->setPen( QPen( Qt::black ) );
    painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignVCenter|Qt::AlignHCenter|Qt::TextWordWrap, index.data().toString(), &r);
    painter->restore();
}

QSize PlayersFragsColumnDelegate::sizeHint( const QStyleOptionViewItem &option, const QModelIndex & ) const {
    return option.rect.size();
}
