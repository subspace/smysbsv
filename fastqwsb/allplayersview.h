/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef ALLPLAYERSVIEW_H
#define ALLPLAYERSVIEW_H

#include <QTableView>

class AllPlayersSortFilterProxyModel;
class AllPlayersModel;
class Player;
class AllPlayersView : public QTableView
{
    Q_OBJECT
public:
    explicit AllPlayersView(QWidget *parent = 0);
    virtual void setModel( QAbstractItemModel *model );

    void SearchPlayer( const QString& text );

signals:
    void SelectedPlayerChanged( int server_index, const Player& player );
    void OnlinePlayerCountChanged( int playerCount );

private:
    AllPlayersModel* model;
    AllPlayersSortFilterProxyModel* proxyModel;
    int lastPlayerCount;
    bool rowChanged;

private slots:
    void RowChanged( const QModelIndex& cur, const QModelIndex& prev );
    void Clicked( const QModelIndex& cur );
    void InnerModelRowCountChanged( const QModelIndex& parent, int start, int end );
};

#endif // ALLPLAYERSVIEW_H
