/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serversmodel.h"
#include "clientsocket.h"
#include "serverinfo.h"
#include "utils.h"
#include <QTimer>
#include <QDirIterator>
#include <QItemDelegate>

ServersModel::ServersModel(QObject *parent) :
    QAbstractItemModel(parent),
    socket( new ClientSocket( this ) ),
    pingSocket( new PingSocket( this ) )
{
    connect( socket, &ClientSocket::ServerList, this, &ServersModel::OnNewServerListArrival );
    connect( socket, &ClientSocket::NewServer, this, &ServersModel::OnNewServerInfoArrival );
    connect( socket, &ClientSocket::connected, this, &ServersModel::OnConnection );
    connect( socket, &ClientSocket::disconnected, this, &ServersModel::OnDisconnection );

    connect( socket, static_cast<void (ClientSocket::*)(ClientSocket::SocketError)>( &ClientSocket::error ), this, &ServersModel::OnConnectionError );

    connect( pingSocket, &PingSocket::ResponseTime, this, &ServersModel::OnPong );
    connect( pingSocket, &PingSocket::Pinging, this, &ServersModel::Pinging );
}

void ServersModel::RefreshServer( const QModelIndex &idx ) {
    if ( !idx.isValid() ) {
        return;
    }

    const ServerAddress &h = servers.at( idx.row() ).host;
    socket->QueryOneServer( h.address, h.port );
}

void ServersModel::Connect( const QHostAddress &address, quint16 port ) {
    socket->abort();
    socket->connectToHost( address, port );
}

void ServersModel::Disconnect( void ) {
    socket->disconnectFromHost();
}

bool ServersModel::IsEmpty( int index ) const {
    const ServerInfo& info = servers.at( index );
    return ( info.numPlayers == 0 && info.numSpectators == 0 );
}

bool ServersModel::IsFull( int index ) const {
    return ( servers.at( index ).numPlayers >= servers.at( index ).maxPlayers );
}

bool ServersModel::IsProxy( int index ) const {
    const ServerInfo& info = servers.at( index );
    return info.isProxy;
}

void ServersModel::PingServers( void ) {
    for ( int i = 0; i < servers.size(); ++i ) {
        const ServerInfo& info = servers.at( i );
        pingSocket->PingServer( info.host.address, info.host.port );
    }
}

void ServersModel::PingServer( const QHostAddress &address, quint16 port ) {
    pingSocket->PingServer( address, port );
}

void ServersModel::RefreshServers( void ) {
    socket->QueryAllServers();
}

void ServersModel::OnConnection( void ) {
    QueryAllServers();
    emit Connected( true );
}

void ServersModel::OnDisconnection( void ) {
    emit Connected( false );
    if ( servers.size() ) {
        beginRemoveRows( QModelIndex(), 0, servers.size()-1 );
        servers.clear();
        endRemoveRows();
        emit ServerListChanged( servers );
    }
}

void ServersModel::OnConnectionError( int ) {
    emit Connected( false );
    emit ConnectionError( socket->errorString() );
}

void ServersModel::QueryAllServers( void ) {
    socket->QueryAllServers();
}

void ServersModel::OnNewServerListArrival( const QList<ServerInfo> &list ) {
    QList<ServerInfo> old = servers;

    beginRemoveRows( QModelIndex(), 0, servers.size()-1 );
    servers.clear();
    endRemoveRows();

    beginInsertRows( QModelIndex(), 0, list.size()-1 );
    servers = list;

    // Don't lose last ping information
    for ( int i = 0; i < servers.size(); ++i ) {
        ServerInfo& info = servers[i];
        info.ping = 999;
//        int idx = old.indexOf( info );
//        if ( idx == -1 ) {
//            continue;
//        }
//        info.ping = old.at( idx ).ping;
    }

    endInsertRows();

    emit ServerListChanged( list );
}

void ServersModel::AddServer( const QHostAddress &addr, quint16 port) {
    socket->QueryOneServer( addr, port );
}

void ServersModel::OnNewServerInfoArrival( const ServerInfo &info ) {
    int idx = servers.indexOf( info );
    if ( idx == -1 ) {
        beginInsertRows( QModelIndex(), servers.size(), servers.size() );
        servers.append( info );
        endInsertRows();
        emit ServerListChanged( servers );
        return;
    }
}

void ServersModel::OnPong( const QHostAddress &addr, quint16 port, quint16 ms ) {
    for ( int i = 0; i < servers.size(); ++i ) {
        ServerInfo& info = servers[i];
        if ( info.host.address == addr && info.host.port == port ) {
            info.ping = ms;
            emit dataChanged( index( i, PingColumn ), index( i, PingColumn ), QVector<int>() << Qt::DisplayRole );
            return;
        }
    }
}

const QList<Player>& ServersModel::Players( const QModelIndex &index ) const {
    return servers.at( index.row() ).players;
}

const QList<Rule>& ServersModel::Rules( const QModelIndex& index ) const {
    return servers.at( index.row() ).rules;
}

const ServerInfo& ServersModel::ServerInformation( const QModelIndex &index ) const {
    return servers.at( index.row() );
}

QVariant ServersModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( role != Qt::DisplayRole || orientation != Qt::Horizontal ) {
        return QAbstractItemModel::headerData( section, orientation, role );
    }

    switch ( section ) {
    case CountryColumn:
        return QVariant( tr( "Country" ) );
    case PingColumn:
        return QVariant( tr( "Lag" ) );
    case PlayersColumn:
        return QVariant( tr( "Players" ) );
    case SpectatorsColumn:
        return QVariant( tr( "Specs" ) );
    case MapColumn:
        return QVariant( tr( "Map" ) );
    case NeedPassColumn:
        return QVariant( tr( "Need Pass?") );
    case AddressColumn:
        return QVariant( tr( "Address" ) );
    case NameColumn:
        return QVariant( tr( "Hostname" ) );
    }
    return QAbstractItemModel::headerData( section, orientation, role );
}

#include <QBrush>
QVariant ServersModel::data( const QModelIndex &index, int role ) const {
    int row = index.row();
    int column = index.column();

    if ( row > servers.size() ) {
        return QVariant();
    }

    const ServerInfo& info = servers.at( row );
    if ( role == Qt::BackgroundRole ) {
        if ( info.state != OnlineState ) {
            return QBrush( Qt::darkYellow );
        }
        if ( info.numPlayers >= info.maxPlayers && info.numSpectators >= info.maxSpectators ) {
            return QBrush( Qt::lightGray );
        }
        return QVariant();
    }

    if ( role == Qt::TextAlignmentRole ) {
        switch ( column ) {
        case PingColumn:
            return (int)Qt::AlignRight | (int)Qt::AlignVCenter;
        case PlayersColumn:
            return (int)Qt::AlignRight | (int)Qt::AlignVCenter;
        case SpectatorsColumn:
            return (int)Qt::AlignRight | (int)Qt::AlignVCenter;
        default:
            break;
        }
    }
    if ( role == SortRole ) {
        switch ( column ) {
        case CountryColumn:
            return QVariant( info.countryCode );
        case PingColumn:
            return QVariant( info.ping );
        case PlayersColumn:
            return QVariant( info.numPlayers );
        case SpectatorsColumn:
            return QVariant( info.numSpectators );
        case MapColumn:
            return QVariant( info.map );
        case NeedPassColumn: {
            int idx = info.rules.indexOf( Rule( "needpass" ) );
            if ( idx == -1) {
                return false;
            } else {
                if ( info.rules.at( idx ).value == "1" ) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        case AddressColumn:
            return QVariant( info.hostname );
        case NameColumn:
            return QVariant( info.name );
        case LastColumn:
            return row; // Needed for the hack to fix the row position
        }
    }

    if ( role == Qt::DisplayRole ) { // UserRole for passing data to sorting
        switch ( column ) {
        case CountryColumn:
            return QVariant();
        case PingColumn:
            if ( info.ping == 0xffff ) {
                return QVariant();
            } else {
                return QVariant( info.ping );
            }
        case PlayersColumn:
            return QVariant( QString("%1 / %2").arg( info.numPlayers ).arg( info.maxPlayers ) );
        case SpectatorsColumn:
            return QVariant( QString("%1 / %2").arg( info.numSpectators ).arg( info.maxSpectators ) );
        case MapColumn:
            return QVariant( info.map );
        case AddressColumn:
            return QVariant( info.hostname );
        case NameColumn:
            return QVariant( info.name );
        }
    }

    return QVariant();
}

QModelIndex ServersModel::index( int row, int column, const QModelIndex & ) const {
    return createIndex( row, column );
}

QModelIndex ServersModel::parent( const QModelIndex & ) const {
    return QModelIndex();
}

int ServersModel::rowCount( const QModelIndex & ) const {
    return servers.size();
}

int ServersModel::columnCount( const QModelIndex & ) const {
    return LastColumn;
}
