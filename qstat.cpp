/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "qstat.h"
#include <QXmlStreamReader>
#include <QFile>
#include <QDir>
#include <QDebug>

QStat::QStat(QObject *parent) :
    QProcess(parent)
{
    connect( this, &QStat::readyReadStandardOutput, this, &QStat::ParseQStatOutput );
    connect( this, static_cast<void (QStat::*)(int, QProcess::ExitStatus)>(&QStat::finished), this, &QStat::Finished );
}

bool QStat::GetInfo( const QList<ServerAddress> addresses ) {
    if ( state() == QProcess::Running || state() == QProcess::Starting || addresses.isEmpty() ) {
        return false;
    }

    data.clear();
    QStringList servers;
    serversPath = QString( "%1/.%2" ).arg( QDir::currentPath() ).arg( QString::number( (qulonglong)this, 16 ) );
    QFile f( serversPath );
    if ( !f.open( QIODevice::WriteOnly ) ) {
        return false;
    }
    foreach ( ServerAddress addr, addresses ) {
        f.write( QString( "QWS %1:%2\n" ).arg( addr.address.toString() ).arg( addr.port ).toLatin1() );
    }
    f.close();

    start( "qstat", QStringList() << "-P"
           << "-hc"
           << "-R"
           << "-xml"
           << "-f"
           << serversPath );

    return true;
}

bool QStat::GetInfoFromMaster( const QList<ServerAddress> addresses ) {
    if ( state() == QProcess::Running || state() == QProcess::Starting || addresses.isEmpty() ) {
        return false;
    }

    data.clear();
    QStringList servers;
    serversPath = QString( "%1/.%2" ).arg( QDir::currentPath() ).arg( QString::number( (qulonglong)this, 16 ) );
    QFile f( serversPath );
    if ( !f.open( QIODevice::WriteOnly ) ) {
        return false;
    }
    foreach ( ServerAddress addr, addresses ) {
        f.write( QString( "QWM %1:%2\n" ).arg( addr.address.toString() ).arg( addr.port ).toLatin1() );
    }
    f.close();

    start( "qstat", QStringList() << "-P"
           << "-hc"
           << "-R"
           << "-xml"
           << "-f"
           << serversPath );
    return true;
}

void QStat::ParseQStatOutput( void ) {
    quint64 startPos = 0, endPos = 0;
    data.append( readAllStandardOutput() );

    QXmlStreamReader parser( data );

    while ( !parser.atEnd() && !parser.hasError() ) {
        QXmlStreamReader::TokenType token = parser.readNext();
        if ( token == QXmlStreamReader::StartDocument ) {
            continue;
        }
        if ( token == QXmlStreamReader::StartElement ) {
            QString element( parser.name().toString() );
            if ( element != "server" ) {
                continue;
            }
            QXmlStreamAttributes attributes = parser.attributes();
            if ( attributes.value( "type" ) == "QWM" ) {
                continue;
            }
            startPos = endPos;
            continue;
        }
        if ( token == QXmlStreamReader::EndElement ) {
            QString element( parser.name().toString() );
            if ( element == "qstat" ) {
                break;
            }
            if ( element != "server" ) {
                continue;
            }
            endPos = parser.characterOffset();

            QByteArray d( data.mid( startPos, endPos - startPos ) );
            ParseServerData( d );
        }
    }
    if ( parser.hasError() ) {
        if ( parser.error() == QXmlStreamReader::PrematureEndOfDocumentError ) {
            data.remove( 0, endPos );
            if ( endPos != 0 ) {
                data.prepend( "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n<qstat>\n" );
            }
            return;
        }
        qDebug() << parser.errorString();
        QFile f("./err.txt");
        f.open(QFile::WriteOnly);
        f.write(data);
        f.close();
        exit(0);
    }
}

void QStat::ParseServerData( const QByteArray &data ) {
    QXmlStreamReader parser( data );
    while ( !parser.atEnd() && !parser.hasError() ) {
        QXmlStreamReader::TokenType token = parser.readNext();
        if ( token == QXmlStreamReader::EndElement && parser.name() == "qstat" ) {
            break;
        }
        if ( token == QXmlStreamReader::StartDocument || token != QXmlStreamReader::StartElement ) {
            continue;
        }
        QString element( parser.name().toString() );
        if ( element != "server" ) {
            continue;
        }
        QXmlStreamAttributes attributes = parser.attributes();
        if ( attributes.value( "type" ) == "QWM" ) {
            continue;
        }
        parser.characterOffset();
        ServerInfo serverInfo;
        QStringList address = attributes.value( "address" ).toString().split( ":" );
        serverInfo.host.address = QHostAddress( address.at( 0 ) );
        serverInfo.host.port = address.at( 1 ).toUShort();
        QString status = attributes.value("status").toString();
        if ( status != "UP" /*status == "TIMEOUT" || status == "DOWN"*/ ) {
            serverInfo.state = OfflineState;
        } else {
            serverInfo.state = OnlineState;
        }
        while ( !parser.atEnd() && !parser.hasError() ) {
            token = parser.readNext();
            if ( token == QXmlStreamReader::EndElement && parser.name() == "server" ) {
                break;
            }
            if ( token != QXmlStreamReader::StartElement ) {
                continue;
            }
            element = parser.name().toString();

            if ( element == "hostname" ) {
                serverInfo.hostname = parser.readElementText().toUtf8();
            } else if ( element == "name" ) {
                serverInfo.name = parser.readElementText().toUtf8();
            } else if ( element == "gametype" ) {
                serverInfo.gameType = parser.readElementText().toUtf8();
            } else if ( element == "map" ) {
                serverInfo.map = parser.readElementText().toUtf8();
            } else if ( element == "numplayers" ) {
                serverInfo.numPlayers = parser.readElementText().toUShort();
            } else if ( element == "maxplayers" ) {
                serverInfo.maxPlayers = parser.readElementText().toUShort();
            } else if ( element == "numspectators" ) {
                serverInfo.numSpectators = parser.readElementText().toUShort();
            } else if ( element == "maxspectators" ) {
                serverInfo.maxSpectators = parser.readElementText().toUShort();
            } else if ( element == "ping" ) {
                serverInfo.ping = parser.readElementText().toUShort();
            } else if ( element == "retries" ) {
                serverInfo.retries = parser.readElementText().toUShort();
            } else if ( element == "rules" ) {
                while ( !parser.atEnd() && !parser.hasError() ) {
                    token = parser.readNext();
                    if ( token == QXmlStreamReader::EndElement && parser.name() == "rules" ) {
                        break;
                    }
                    if ( token != QXmlStreamReader::StartElement ) {
                        continue;
                    }
                    element = parser.name().toString();
                    if ( element != "rule" ) {
                        continue;
                    }
                    attributes = parser.attributes();
                    Rule rule;
                    rule.name = attributes.value( "name" ).toUtf8();
                    rule.value = parser.readElementText().toUtf8();
                    serverInfo.rules.push_back( rule );
                }
            } else if ( element == "players" ) {
                while ( !parser.atEnd() && !parser.hasError() ) {
                    token = parser.readNext();
                    if ( token == QXmlStreamReader::EndElement && parser.name() == "players" ) {
                        break;
                    }
                    if ( token != QXmlStreamReader::StartElement ) {
                        continue;
                    }

                    element = parser.name().toString();
                    if ( element != "player" ) {
                        continue;
                    }
                    attributes = parser.attributes();

                    Player player;
                    player.number = attributes.value( "number" ).toUShort();
                    while ( !parser.atEnd() && !parser.hasError() ) {
                        token = parser.readNext();
                        if ( token == QXmlStreamReader::EndElement && parser.name() == "player" ) {
                            break;
                        }
                        if ( token != QXmlStreamReader::StartElement ) {
                            continue;
                        }

                        element = parser.name().toString();
                        attributes = parser.attributes();
                        if ( element == "name" ) {
                            player.name = parser.readElementText().toUtf8();
                        } else if ( element == "score" ) {
                            player.score = parser.readElementText().toShort();
                        } else if ( element == "time" ) {
                            player.time = parser.readElementText().toUShort();
                        } else if ( element == "color" ) {
                            QString f( attributes.value( "for" ).toString() );
                            bool ok;
                            if ( f == "shirt" ) {
                                player.shirtColor = parser.readElementText().replace( "#", "0x" ).toULong( &ok, 16);
                            } else if ( f == "pants" ) {
                                player.pantsColor = parser.readElementText().replace( "#", "0x" ).toULong( &ok, 16);
                            }
                        } else if ( element == "ping" ) {
                            player.ping = parser.readElementText().toUShort();
                        } else if ( element == "skin" ) {
                            player.skin = parser.readElementText().toUtf8();
                        } else if ( element == "team" ) {
                            player.team = parser.readElementText().toUtf8();
                        }
                    }
                    serverInfo.players.push_back( player );
                }
            }
        }
        emit ServerData( serverInfo );
    }
}

void QStat::Finished( int , QProcess::ExitStatus ) {
    QFile::remove( serversPath );
}
