/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "server.h"
#include "serverconnection.h"
#include "serverinfo.h"
#include "protocol.h"
#include <QDataStream>
#include <QDebug>

Server::Server(QObject *parent) :
    QTcpServer(parent)
{
}

void Server::incomingConnection( qintptr handle ) {
    ServerConnection* client = new ServerConnection( this );
    client->setSocketDescriptor( handle );
    connect( client, &ServerConnection::disconnected, this, &Server::ClientDisconnected );
    connections.push_back( client );
    qDebug() << "Client connected" << client->peerAddress().toString();
}

void Server::ClientDisconnected( void ) {
    ServerConnection* client = qobject_cast<ServerConnection*>( sender() );
    connections.removeAll( client );
    client->deleteLater();
    qDebug() << "Client dropped" << client->peerAddress().toString();
}

void Server::NotifyClientsOfANewServer( const ServerInfo &info ) {
    qDebug() << "New Server" << info.host.address << info.host.port;

    QByteArray out;
    QDataStream stream( &out, QIODevice::WriteOnly );
    stream << (quint32)0 << (quint8)SvcNewServer << info;
    stream.device()->seek( 0 );
    stream << (quint32)( out.size() - sizeof( quint32 ) );

    foreach ( ServerConnection* client, connections ) {
        if ( client->liveMode ) {
            client->write( out );
        }
    }
}

