#-------------------------------------------------
#
# Project created by QtCreator 2013-10-05T22:33:09
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = smysbsv
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qstat.cpp \
    serverinfo.cpp \
    serverconnection.cpp \
    app.cpp \
    server.cpp \
    qtvbrowser.cpp \
    utils.cpp

HEADERS += \
    protocol.h \
    qstat.h \
    test.h \
    serverinfo.h \
    serverconnection.h \
    app.h \
    server.h \
    qtvbrowser.h \
    utils.h

DEFINES += __FSBDAEMON__
unix|win32: LIBS += -lGeoIP

