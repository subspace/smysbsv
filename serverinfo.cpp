/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serverinfo.h"
#include <QDataStream>

QDataStream& operator<<( QDataStream& out, const ServerAddress& address ) {
    out << address.address << address.port;
    return out;
}

QDataStream& operator<<( QDataStream& out, const QTVServerAddress& address ) {
    out << address.address << address.port << address.name;
    return out;
}

QDataStream& operator<<( QDataStream& out, const QTVStream& stream ) {
    out << stream.host << stream.streamNumber;
    return out;
}

QDataStream& operator<<( QDataStream& out, const ServerInfo& info ) {
    out << info.host
        << info.countryCode
        << info.hostname
        << info.name
        << info.gameType
        << info.map
        << info.numPlayers
        << info.maxPlayers
        << info.numSpectators
        << info.maxSpectators
        << info.state
        << info.rules
        << info.players
        << info.isProxy
        << info.isQTV
        << info.qtvStreams;
    return out;
}

QDataStream& operator<<( QDataStream& out, const Rule& rule ) {
    out << rule.name
        << rule.value;
    return out;
}

QDataStream& operator<<( QDataStream& out, const Player& player ) {
    out << player.number
        << player.name
        << player.score
        << player.time
        << player.shirtColor
        << player.pantsColor
        << player.ping
        << player.skin
        << player.team;
    return out;
}

QDataStream& operator>>( QDataStream& in, ServerAddress& address ) {
    in >> address.address >> address.port;
    return in;
}

QDataStream& operator>>( QDataStream& in, QTVServerAddress& address ) {
    in >> address.address >> address.port >> address.name;
    return in;
}

QDataStream& operator>>( QDataStream& in, QTVStream& stream ) {
    in >> stream.host >> stream.streamNumber;
    return in;
}

QDataStream& operator>>( QDataStream& in, ServerInfo& info ) {
    in  >> info.host
        >> info.countryCode
        >> info.hostname
        >> info.name
        >> info.gameType
        >> info.map
        >> info.numPlayers
        >> info.maxPlayers
        >> info.numSpectators
        >> info.maxSpectators
        >> info.state
        >> info.rules
        >> info.players
        >> info.isProxy
        >> info.isQTV
        >> info.qtvStreams;
    return in;
}

QDataStream& operator>>( QDataStream& in, Rule& rule ) {
    in  >> rule.name
        >> rule.value;
    return in;
}

QDataStream& operator>>( QDataStream& in, Player& player ) {
    in  >> player.number
        >> player.name
        >> player.score
        >> player.time
        >> player.shirtColor
        >> player.pantsColor
        >> player.ping
        >> player.skin
        >> player.team;
    return in;
}
