/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk <luiz@netdome.biz>. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERINFO_H
#define SERVERINFO_H

#include <QByteArray>
#include <QHostAddress>
#include <QDateTime>

class QDataStream;

struct ServerAddress {
    ServerAddress( const QHostAddress& address = QHostAddress(), quint16 port = 27500 ):
        address( address ),
        port( port ) {}

    bool operator ==( const ServerAddress& other ) const {
        return ( other.address == this->address && other.port == this->port );
    }

    bool operator <( const ServerAddress& other ) const {
        if ( other.address == this->address && other.port == this->port ) {
            return false;
        }

        if ( other.address == this->address ) {
            return ( this->port < other.port );
        }

        if ( other.port == this->port ) {
            return ( this->address.toIPv4Address() < other.address.toIPv4Address() );
        }
        return ( this->address.toIPv4Address() < other.address.toIPv4Address() );
    }

    QHostAddress address;
    quint16 port;
};

struct QTVServerAddress {
    QTVServerAddress( const QHostAddress& address = QHostAddress(), quint16 port = 27500, const QString& name = "" ):
        address( address ),
        port( port ),
        name( name ) {}

    bool operator ==( const QTVServerAddress& other ) const {
        return ( other.address == this->address && other.port == this->port );
    }

    bool operator <( const QTVServerAddress& other ) const {
        if ( other.address == this->address && other.port == this->port ) {
            return false;
        }

        if ( other.address == this->address ) {
            return ( this->port < other.port );
        }

        if ( other.port == this->port ) {
            return ( this->address.toIPv4Address() < other.address.toIPv4Address() );
        }
        return ( this->address.toIPv4Address() < other.address.toIPv4Address() );
    }

    QHostAddress address;
    quint16 port;
    QString name;
};

struct QTVStream {
    quint16 streamNumber;
    QTVServerAddress host;

    bool operator ==( const QTVStream& other ) const {
        return ( /*other.streamNumber == streamNumber &&*/ other.host == host );
    }
};

struct Rule {
    Rule( const QByteArray& name = QByteArray(), const QByteArray& value = QByteArray() ):
        name( name ),
        value( value )
    {}

    QByteArray name;
    QByteArray value;

    bool operator ==( const Rule& other ) const {
        return ( other.name == this->name );
    }
};

struct Player {
    quint16 number;
    QByteArray name;
    qint16 score;
    quint16 time;
    quint32 shirtColor;
    quint32 pantsColor;
    quint16 ping;
    QByteArray skin;
    QByteArray team;

    bool operator ==( const Player& other ) const {
        return ( this->number == other.number &&
                 this->name == other.name &&
                 this->score == other.score &&
                 this->time == other.time &&
                 this->shirtColor == other.shirtColor &&
                 this->pantsColor == other.pantsColor &&
                 this->ping == other.ping &&
                 this->skin == other.skin &&
                 this->team == other.team );
    }
};

enum ServerState {
    OnlineState,
    OfflineState,
    ApprovalState, // Server not yet being broadcast to clients cos it's under test to see if its an online server or what
    DeadState
};

struct ServerInfo {
    ServerInfo( void ):
        numPlayers( 0 ),
        maxPlayers( 0 ),
        numSpectators( 0 ),
        maxSpectators( 0 ),
        ping( 0xffff ),
        retries( 0 ),
        state( OfflineState ),
        isQTV( false )
    {
    }

    ServerAddress host;
    QByteArray countryCode;
    QByteArray hostname;
    QByteArray name;
    QByteArray gameType;
    QByteArray map;
    quint16 numPlayers;
    quint16 maxPlayers;
    quint16 numSpectators;
    quint16 maxSpectators;
    quint16 ping;
    quint8 retries;
    quint8 state;
    QList<Rule> rules;
    QList<Player> players;
    quint8 isQTV;
    quint8 isProxy;
    /* QTVs that stream this server */
    QList<QTVStream> qtvStreams;

    bool operator ==( const ServerInfo& other ) const {
        return ( other.host == this->host );
    }

#ifdef __FSBDAEMON__
    /* Internal */
    QDateTime lastUpdate;
    bool queued;
    int  numRetries; // New server in offline state uses this counter


#endif
};

QDataStream& operator<<( QDataStream& out, const ServerInfo& info );
//QDataStream& operator<<( QDataStream& out, const Rule& rule );
//QDataStream& operator<<( QDataStream& out, const Player& player );
QDataStream& operator>>( QDataStream& in, ServerInfo& info );
//QDataStream& operator>>( QDataStream& in, Rule& rule );
//QDataStream& operator>>( QDataStream& in, Player& player );

#endif // SERVERINFO_H
