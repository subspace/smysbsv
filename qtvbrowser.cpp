/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "qtvbrowser.h"
#include <QString>
#include <QStringList>
#include <QHostInfo>
#include <QRegExp>

QTVBrowser::QTVBrowser(QObject *parent) :
    QTcpSocket(parent)
{
    connect( this, &QTVBrowser::connected, this, &QTVBrowser::OnConnection );
    connect( this, &QTVBrowser::disconnected, this, &QTVBrowser::OnDisconnection );
    connect( this, &QTVBrowser::readyRead, this, &QTVBrowser::OnDataArrival );
    connect( this, static_cast<void ( QTVBrowser::* )( QTVBrowser::SocketError )>( &QTVBrowser::error ), this, &QTVBrowser::OnError );
}

void QTVBrowser::List( const QString &name, const QHostAddress &address, quint16 port ) {
    connectToHost( address, port );
    qtvName = name;
    qtvAddress = address;
    qtvPort = port;
    command = ListCommand;
    qtvdata.clear();
}

void QTVBrowser::OnConnection( void ) {
    QString header("QTV\n");
    write( header.toUtf8() );

    switch ( command ) {
    case ListCommand:
        write( "SOURCELIST\n" );
        break;
    default:
        break;
    }

    write( "\n" ); //End of session
}

void QTVBrowser::OnDisconnection( void ) {
}

void QTVBrowser::OnError( QTcpSocket::SocketError ) {
    // We don't care just send and empty list
    emit SourceList( QList<QTVBrowser::Source>() );
}

void QTVBrowser::OnDataArrival( void ) {
    qtvdata.push_back( readAll() );
    if ( qtvdata.size() < 3 )
        return;

    // Look for end of transmission
    int index = qtvdata.indexOf( "\n\n", qtvdata.size()-3 );
    if ( index != -1 ) {
        disconnectFromHost();
        qtvdata.resize( index );
        qDebug() << "QTV Source List received. Parsing..." << qtvName;
        ParseData();
    }
}

static QRegExp regexSourcelist( "^ASOURCE:\\s*([0-9]+):\\s*tcp:([a-z0-9\\.]+):([0-9]+):.*$" );

void QTVBrowser::ParseData( void ) {
    if ( command != ListCommand )
        return;

    QStringList sourceslist = QString( qtvdata ).split( '\n' );
    foreach ( QString line, sourceslist ) {
        int idx = regexSourcelist.indexIn( line );
        if ( idx == -1 ) {
            continue;
        }
        int streamNumber = regexSourcelist.cap( 1 ).toInt();
        QString hostname = regexSourcelist.cap( 2 );
        quint16 port     = regexSourcelist.cap( 3 ).toUShort();

        Source s;
        s.streamNumber = streamNumber;
        s.host.port = port;
        sources.push_back( s );
        Source* psource = &sources[ sources.size() - 1 ];

        QHostAddress addr;

        // Try the easy way first
        bool valid = addr.setAddress( hostname );
        if ( valid ) {
            psource->host.address = addr;
            continue;
        }

        // Check if we have it on the cached hostnames
        if ( !resolvedHosts.isEmpty() ) {
            addr = resolvedHosts.value( hostname );
            if ( !addr.isNull() ) {
                psource->host.address = addr;
                continue;
            }
        }

        // We have to resolve this host :(
        if ( !activeLookups.contains( hostname ) ) {
            QHostInfo::lookupHost( hostname, this, SLOT( LookedUpHost(QHostInfo) ) );
        }
        activeLookups.insertMulti( hostname, psource );
    }
    if ( !activeLookups.size() ) {
        emit SourceList( sources );
    }
}

void QTVBrowser::LookedUpHost( const QHostInfo &host ) {
    if ( host.error() != QHostInfo::NoError ) {
        return;
    }

    QString hostname = host.hostName();
    QHostAddress addr = host.addresses().first();
    foreach ( Source* source, activeLookups.values( hostname ) ) {
        source->host.address = addr;
    }
    resolvedHosts.insert( hostname, addr );
    activeLookups.remove( hostname );

    if ( !activeLookups.size() ) {
        emit SourceList( sources );
    }
}
