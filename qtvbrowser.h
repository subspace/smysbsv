/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef QTVBROWSER_H
#define QTVBROWSER_H

#include <QTcpSocket>
#include <QHostAddress>
#include "serverinfo.h"

class QHostInfo;
class QTVBrowser : public QTcpSocket
{
    Q_OBJECT
public:
    struct Source {
        bool operator ==( const Source& other ) const {
            return ( streamNumber == other.streamNumber && host == other.host );
        }

        int           streamNumber;
        ServerAddress host;
    };

    explicit QTVBrowser(QObject *parent = 0);
    void List( const QString& name, const QHostAddress& address, quint16 port ); // Lists the streams in this QTV server.
    const QHostAddress QTVServerAddress( void ) const { return qtvAddress; }
    quint16 QTVServerPort( void ) const { return qtvPort; }
    const QString& QTVName() const { return qtvName; }

signals:
    void SourceList( const QList<QTVBrowser::Source>& sources );

private:
    enum {
        ListCommand,
        LastCommand
    };
    int command;
    QByteArray qtvdata;
    QMap<QString, QHostAddress> resolvedHosts;
    QMap<QString, Source*> activeLookups;
    QList<Source> sources;
    QString qtvName;
    QHostAddress qtvAddress;
    quint16 qtvPort;

    void OnConnection( void );
    void OnDisconnection( void );
    void OnError( QTVBrowser::SocketError );
    void OnDataArrival( void );
    void ParseData( void );

private slots:
    void LookedUpHost( const QHostInfo& host );
};

#endif // QTVBROWSER_H
