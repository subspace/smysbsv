/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef QSTAT_H
#define QSTAT_H

#include <QProcess>
#include <QHostAddress>
#include "serverinfo.h"

class QStat : public QProcess
{
    Q_OBJECT
public:
    explicit QStat(QObject *parent = 0);
    bool GetInfoFromMaster( const QList<ServerAddress> addresses );
    bool GetInfo( const QList<ServerAddress> addresses );

signals:
    void ServerData( const ServerInfo& info );

private:
    QByteArray data;
    QString serversPath;

    void ParseQStatOutput( void );
    void ParseServerData( const QByteArray& data );
private slots:
    void Finished( int exitCode , QProcess::ExitStatus exitStatus );
};

#endif // QSTAT_H
