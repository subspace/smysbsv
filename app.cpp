/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "app.h"
#include "protocol.h"
#include "server.h"
#include "utils.h"
#include "qtvbrowser.h"
#include <QTcpSocket>
#include <QDataStream>
#include <QSettings>
#include <QHostAddress>
#include <QHostInfo>
#include <QDebug>

#include <QTimer>

#include <QFile>
#include <GeoIPCity.h>

App* app;

void App::Test( void ) {
}

App::App( int &argc , char **argv ) :
    QCoreApplication( argc, argv ),
    server( new Server( this ) ),
    settings( new QSettings( "./settings.cfg", QSettings::NativeFormat, this ) ),
    geoIP( 0 ) {
    connect( &qstat, &QStat::ServerData, this, &App::OnServerDataArrival );
    connect( &qstat, static_cast<void (QStat::*)(int, QProcess::ExitStatus)>(&QStat::finished), this, &App::QStatRunNewCommand );
    connect( &dailyTimer, &QTimer::timeout, this, &App::OnRefreshMasters );
    connect( &dailyTimer, &QTimer::timeout, this, &App::OnRefreshQTVServers );
    connect( &serversRefreshTimer, &QTimer::timeout, this, &App::OnRefreshServers );
    settings->setValue( "bind_address", settings->value("bind_address", "0.0.0.0") );
    settings->setValue( "bind_port", settings->value("bind_port", 27500) );
    app = this;
}

bool App::Init( void ) {
    qDebug() << "smysb server v1.0";

    geoIP = GeoIP_new( GEOIP_MEMORY_CACHE );
    if ( !geoIP ) {
        return false;
    }

    LoadMastersFromFile( App::applicationDirPath() + "/masters.txt" );
    LoadServersFromFile( App::applicationDirPath() + "/servers.txt" );
    
    OnRefreshMasters();
    dailyTimer.start( 86400000 );
    serversRefreshTimer.start( 500 );
    QHostAddress bindAddress( settings->value( "bind_address", "0.0.0.0" ).toString() );
    quint16 bindPort( settings->value( "bind_port", 27500 ).toUInt() );
    bool noError = server->listen( bindAddress, bindPort );
    if ( !noError ) {
        qDebug() << "Failed to open:" << bindAddress << bindPort;
    }
    return noError;
}

const QList<ServerInfo>& App::Servers( void ) {
    return servers;
}

void App::AddMaster( const QHostAddress &address, quint16 port ) {
    ServerAddress master;
    master.address = address;
    master.port = port;
    masters.push_back( master );
    qDebug() << "Added master" << address << port;
}

void App::OnRefreshMasters( void ) {
    foreach ( ServerAddress m, masters ) {
        QStatAddToQueue( MastersQueryCommand, m );
    }
    QStatStart();

    foreach ( ServerInfo info, servers ) {
        if ( info.isQTV ) {
            QTVBrowser* b = new QTVBrowser( this );
            b->List( info.name, info.host.address, info.host.port );
        }
    }
}

void App::OnRefreshServers( void ) {
    QDateTime curDateTime = QDateTime::currentDateTime();

    for ( int i = 0; i < servers.size(); ++i ) {
        ServerInfo &info = servers[i];
        if ( info.queued ) {
            continue;
        }
        qint64 secs = info.lastUpdate.secsTo( curDateTime );
        if ( secs < serversRefreshInterval ) {
            continue;
        }
        QStatAddToQueue( ServersQueryCommand, info.host );
        info.queued = true;
    }
    QStatStart();
}

void App::QTVRequestSourceList( const ServerInfo &info ) {
    QTVBrowser* b = new QTVBrowser( this );
    connect( b, &QTVBrowser::SourceList, this, &App::OnQTVSourceListArrival );
    b->List( info.name, info.host.address, info.host.port );
}

void App::OnRefreshQTVServers( void ) {
    for ( int i = 0; i < servers.size(); ++i ) {
        const ServerInfo& info = servers.at( i );
        if ( info.isQTV ) {
            QTVRequestSourceList( info );
        }
    }
}

void App::OnQTVSourceListArrival( const QList<QTVBrowser::Source> &sources ) {
    QTVBrowser* browser = qobject_cast<QTVBrowser*>( sender() );
    QTVServerAddress addr( browser->QTVServerAddress(), browser->QTVServerPort(), browser->QTVName() );
    qtvCachedSources.remove( addr );
    for ( int i = 0; i < sources.size(); ++i ) {
        qtvCachedSources.insertMulti( addr, sources.at( i ) );
    }
    browser->deleteLater();

    QTVUpdateServersStreamAddressFromCache();
    QTVUpdateServersListFromCache();
}

void App::QTVUpdateServerStreamAddressFromCache( ServerInfo &info ) {
    const QList<QTVBrowser::Source> values = qtvCachedSources.values();
    const QList<QTVServerAddress> keys = qtvCachedSources.keys();
    for ( int i = 0; i < values.size(); ++i ) {
        const QTVServerAddress&     qtvServer = keys.at( i );
        const QTVBrowser::Source&   qtvSource = values.at( i );

        if ( qtvSource.host == info.host ) {
            QTVStream qs;
            qs.host = qtvServer;
            qs.streamNumber = qtvSource.streamNumber;

            info.qtvStreams.removeAll( qs );
            info.qtvStreams.push_back( qs );
            continue;
        }
    }
}

void App::QTVUpdateServersListFromCache( void ) {
    const QList<QTVBrowser::Source>& sources = qtvCachedSources.values();
    for ( int i = 0; i < sources.size(); ++i ) {
        const ServerAddress& host = sources.at( i ).host;
        // This server is not on the list yet, request it!
        if ( !IsServerOnTheList( host ) ) {
            qDebug() << "New server from QTV" << host.address << host.port;
            QStatAddToQueue( ServersQueryCommand, host );
        }
    }
}

void App::QTVUpdateServersStreamAddressFromCache( void ) {
    for ( int i = 0; i < servers.size(); ++i ) {
        QTVUpdateServerStreamAddressFromCache( servers[ i ] );
    }
}

void App::QStatAddToQueue( QStatCommandType cmd, const ServerAddress &server ) {
    if ( cmd == ServersQueryCommand ) {
        for ( int i = 0; i < qstatQueue.size(); ++i ) {
            QStatQueueItem& item = qstatQueue[i];
            if ( item.command == ServersQueryCommand ) {
                if ( item.addresses.contains( server ) ) {
                    return;
                }
                item.addresses.push_back( server );
                return;
            }
        }
    }
    qstatQueue.push_back( QStatQueueItem( cmd, QList<ServerAddress>() << server ) );
}

void App::QStatStart( void ) {
    if ( qstat.state() == QStat::Starting || qstat.state() == QStat::Running ) {
        return;
    }
    QStatRunNewCommand();
}

void App::QStatRunNewCommand( void ) {
    if ( qstatQueue.isEmpty() ) {
        return;
    }

    QStatQueueItem item = qstatQueue.takeAt( 0 );
    if ( item.command == MastersQueryCommand ) {
        qstat.GetInfoFromMaster( item.addresses );
        return;
    }
    if ( item.command == ServersQueryCommand ) {
        qstat.GetInfo( item.addresses );
        return;
    }
}

void App::OnServerDataArrival( const ServerInfo &remoteInfo ) {
    QDateTime curTime = QDateTime::currentDateTime();
    QVector<ServerInfo> forRemoval;

    for ( int i = 0; i < servers.size(); ++i ) {
        ServerInfo& localInfo = servers[i];
        if ( localInfo == remoteInfo ) {
            // Remove from next queue to be ran 'cos we just received this server.
            for ( int i = 0; i < qstatQueue.size(); ++i ) {
                QStatQueueItem& item = qstatQueue[i];
                if ( item.command != ServersQueryCommand ) {
                    continue;
                }

                int index = item.addresses.indexOf( localInfo.host );
                if ( index == -1 ) {
                    break;
                }
                item.addresses.removeAt( index );
                break;
            }

            // Some settings need to be copied over to the new struct
            int state;
            int retries;
            bool isNew = false;

            if ( remoteInfo.state == OfflineState ) {
                retries = ++localInfo.numRetries;
                if ( retries >= 10 ) {
                    state = DeadState;
                } else {
                    state = localInfo.state;
                }
            } else {
                retries = 0;
                state = remoteInfo.state; // Which is the OnlineState
                isNew = ( localInfo.state == ApprovalState );
            }
            QList<QTVStream> streams = localInfo.qtvStreams;

            bool isProxy = localInfo.isProxy;
            bool isQTV = localInfo.isQTV;
            QByteArray  countryCode( localInfo.countryCode );

            localInfo = remoteInfo;
            localInfo.numRetries = retries;
            localInfo.state = state;
            localInfo.countryCode = countryCode;
            localInfo.lastUpdate = curTime;
            localInfo.queued = false;
            localInfo.qtvStreams = streams;
            localInfo.isProxy = isProxy;
            localInfo.isQTV = isQTV;

            if ( localInfo.state == DeadState ) { // Now that we've broadcasted his DeadState we can remove
                qDebug() << "Dead server removed" << localInfo.hostname;
                servers.removeAll( localInfo );
            } else {
                if ( isNew ) {
//                    server->NotifyClientsOfANewServer( localInfo );
                }
            }
            return;
        }
    }

    // New server
    servers.push_back( remoteInfo );
    ServerInfo &f = servers.last();
    if ( remoteInfo.state == OfflineState ) { // Already count this as 1 retry
        f.numRetries = 1;
        f.state = ApprovalState; // Enter approval state
    }
    f.lastUpdate = curTime;
    f.queued = false;
    f.countryCode = GeoIP_country_code_by_ipnum( geoIP, f.host.address.toIPv4Address() );
    f.isQTV = Utils::IsQTV( f );
    if ( f.isQTV ) {
        f.isProxy = true;
        QTVRequestSourceList( f );
        qDebug() << "Requesting source list from" << f.name;
    } else {
        f.isProxy = Utils::IsProxy( f );
        QTVUpdateServerStreamAddressFromCache( f );
    }
    if ( f.state == OnlineState ) {
//        server->NotifyClientsOfANewServer( f );
    }
}

bool App::FindServer( const QHostAddress &address, quint16 port, ServerInfo &out ) {
    foreach ( const ServerInfo& info, servers ) {
        if ( info.host.address == address && info.host.port == port ) {
            out = info;
            return true;
        }
    }
    return false;
}

bool App::IsServerOnTheList( const ServerAddress& host ) {
    foreach ( const ServerInfo& info, servers ) {
        if ( info.host == host ) {
            return true;
        }
    }
    return false;
}

void App::LoadMastersFromFile( const QString &mastersFile ) {
    QFile file( mastersFile );
    if ( !file.open( QFile::ReadOnly ) ) {
        qDebug() << "Couldn't open masters file." << mastersFile;
        return;
    }
    while ( !file.atEnd() ) {
        QByteArray line( file.readLine() );
        QList<QString> server = QString( line ).split( ':' );
        QHostInfo hostInfo( QHostInfo::fromName( server.first() ) );
        if ( hostInfo.error() != QHostInfo::NoError )
            continue;
        quint16 port( server.last().toUShort() );
        AddMaster( hostInfo.addresses().first(), port );
    }
}

void App::LoadServersFromFile( const QString &serversFile ) {
    QFile file( serversFile );
    if ( !file.open( QFile::ReadOnly ) ) {
        qDebug() << "Couldn't open servers file." << serversFile;
        return;
    }
    while ( !file.atEnd() ) {
        QByteArray line( file.readLine() );
        QList<QString> server = QString( line ).split( ':' );
        QHostInfo hostInfo( QHostInfo::fromName( server.first() ) );
        if ( hostInfo.error() != QHostInfo::NoError )
            continue;
        quint16 port( server.last().toShort() );
        qDebug() << "Added server" << hostInfo.addresses().first() << port;
        QStatAddToQueue( ServersQueryCommand, ServerAddress( hostInfo.addresses().first(), port ) );
    }
}
