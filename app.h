/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef APP_H
#define APP_H

#include <QCoreApplication>
#include "qstat.h"
#include "qtvbrowser.h"
#include <QTimer>

class Server;
class QSettings;
class QFile;

typedef struct GeoIPTag GeoIP;

enum QStatCommandType { MastersQueryCommand, ServersQueryCommand };
struct QStatQueueItem {
    QStatQueueItem( QStatCommandType command, const QList<ServerAddress>& addresses ):
        command( command ),
        addresses( addresses ) {}
    quint8 command;
    QList<ServerAddress> addresses;
};

class App : public QCoreApplication {
    Q_OBJECT

public:
    explicit App( int &argc, char **argv );
    bool Init( void );
    bool FindServer( const QHostAddress& address, quint16 port, ServerInfo& out );
    bool IsServerOnTheList( const ServerAddress& host );
    void QStatAddToQueue( QStatCommandType cmd, const ServerAddress& server );
    void QStatStart( void );
    const QList<ServerInfo>& Servers( void );

private:
    Server*                 server;
    QSettings*              settings;
    QStat                   qstat;
    QList<ServerInfo>       servers;
    QList<ServerAddress>    masters;
    QList<QStatQueueItem>   qstatQueue;
    QMap<QTVServerAddress, QTVBrowser::Source> qtvCachedSources;
    QTimer                  dailyTimer;
    QTimer                  serversRefreshTimer;
    GeoIP*                  geoIP;

    static const int serversRefreshInterval = 20;

    void AddMaster( const QHostAddress& address, quint16 port = 27000 );
    void LoadMastersFromFile( const QString& mastersFile );
    void LoadServersFromFile( const QString& serversFile );
    void QStatRunNewCommand( void );
    void QTVRequestSourceList( const ServerInfo& info ); // List sources from this qtv server
    void QTVUpdateServersStreamAddressFromCache( void );
    void QTVUpdateServerStreamAddressFromCache( ServerInfo& info );
    void QTVUpdateServersListFromCache( void );

private slots:
    void OnRefreshMasters( void );
    void OnRefreshServers( void );
    void OnRefreshQTVServers( void );
    void OnQTVSourceListArrival( const QList<QTVBrowser::Source>& sources );
    void OnServerDataArrival( const ServerInfo& info );
    void Test( void );
};

extern App *app;
#endif // SERVER_H
