/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "serverconnection.h"
#include "serverinfo.h"
#include "protocol.h"
#include "app.h"
#include <QDataStream>

ServerConnection::ServerConnection(QObject *parent) :
    QTcpSocket(parent),
    dataSize( 0 ),
    liveMode( false )
{
    connect( this, &ServerConnection::readyRead, this, &ServerConnection::ReadPackets );
}

void ServerConnection::ReadPackets( void ) {
    for (;;) {
        // Don't read unless we've got it all
        QDataStream stream( this );
        if ( !dataSize ) {
            if ( bytesAvailable() < sizeof( quint32 ) ) {
                return;
            }
            stream >> dataSize;
        }
        if ( bytesAvailable() < dataSize ) {
            return;
        }

        quint32 expectedBytes = bytesAvailable() - dataSize;
        for (;;) {
            if ( bytesAvailable() <= expectedBytes ) {
                dataSize = 0;
                break;
            }
            if ( stream.atEnd() ) {
                dataSize = 0;
                return;
            }
            quint8 c;
            stream >> c;
            switch( c ) {
            case ClcGet: {
                QHostAddress address;
                quint16 port;
                stream >> address >> port;
                ServerInfo info;
                if ( !app->FindServer( address, port, info ) ) {
                    app->QStatAddToQueue( ServersQueryCommand, ServerAddress( address, port ) );
                    app->QStatStart();
                    break;
                }
                QByteArray out;
                QDataStream outStream( &out, QIODevice::WriteOnly );
                outStream << (quint32)0 << (quint8)SvcInfo << info;
                outStream.device()->seek( 0 );
                outStream << (quint32)( out.size() - sizeof( quint32 ) );
                write( out );
                break;
            }

            case ClcGetAll: {
                QList<ServerInfo> infos;
                for ( int i = 0; i < app->Servers().size(); ++i ) {
                    const ServerInfo& info = app->Servers().at( i );
                    if ( info.state != ApprovalState ) {
                        infos.push_back( info );
                    }
                }
                QByteArray out;
                QDataStream outStream( &out, QIODevice::WriteOnly );
                outStream << (quint32)0 << (quint8)SvcInfoAll << infos;
                outStream.device()->seek( 0 );
                outStream << (quint32)( out.size() - sizeof( quint32 ) );
                write( out );
                break;
            }

            case ClcDisconnect:
            default: {
                qDebug() << "BAD READ ON SERVER";
                disconnectFromHost();
                break;
            }
            }
        }
        dataSize = 0;
    }

}
